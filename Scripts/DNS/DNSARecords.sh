#!/bin/bash
#Author - Johannes Tammekänd
# Date - 07.01.2015
# A script that makes A records for DNS zone files

export LC_ALL=C

for ((i=1; i<=40; i++)); do
	echo "router$i	IN	A	10.2.$((10+$i)).1"
done
