#!/bin/bash
clear
until [ "$selection" = "0" ]; do
    echo " "
    echo "avail_kali"
    echo "avail_hosting"
    echo "avail_firewall"
    echo "is_backup"
    echo " "
    echo "func_gov              <- functionality test (check if forms work)"
    echo "func_shop             <- functionality test (check if forms work)"
    echo "ddos_attack           <- DANGEROUS, DDoS attack"
    echo "ssl                   <- is SSL secure?"
    echo "command_injection     <- checks cmdx possibility (deface only once)"
    echo "xss                   <- inject <script> and curl it out"
    echo "path_traversal        <- tries to access shopX from govX"
    echo "cgi                   <- shellshock"
    echo "sql_injection         <- change shopX prices to 0.000"
    echo "unsecure_cookies      <- check if unsecure"
    echo "cookies_xss           <- http cookie security"
    echo " "
    echo "[clear_all_scores]    <- DANGEROUS erase all scores on this management and scoring machines"
    echo " "
    echo "TIPS:"
    echo "1. Ctrl + C on a task ONLY if program is SLEEPing"
    echo "2. If scoreboard is white you need to find a json mistake"
    echo "   in both the scoreboard ui/json/ files and in your local"
    echo "   Scripts/Scoring/taskXXX/results/ folder and fix it."
    echo "3. clear_all_scores is only safe when checks are not running."
    echo "4. clear_all_scores has to be run in each management station you run checks on "
    echo "5. firewall needs manual ssh to answer 'yes' just once"
    echo " "
    read selection
    echo " "
    case $selection in
        avail_kali )        (cd availability; fab --warn-only run_kali); exit ;;
        avail_hosting )     (cd availability; fab --warn-only run_hosting); exit ;;
        avail_firewall )    (cd availability; fab --warn-only run_fw); exit ;;
        is_backup )         (cd taskBackup; fab --warn-only run_backup); exit ;;

        func_gov )          (cd Watir; fab --warn-only run_gov); exit ;;
        func_shop )         (cd Watir; fab --warn-only run_shop); exit ;;
        ddos_attack )       (cd ddos; fab --warn-only run_ddos); exit ;;
        ssl )               (cd ssltest; fab --warn-only run_SSL); exit ;;
        command_injection ) (cd deface; fab --warn-only run_gov); exit ;;
        sql_injection )     (cd deface; fab --warn-only run_shop); exit ;;
        xss )               (cd xss; fab --warn-only run_xss); exit ;;
        path_traversal )    (cd xss; fab --warn-only run_path); exit ;;
        cgi )               (cd shellshock; fab --warn-only run_ss); exit ;;
        unsecure_cookies )  (cd cookies; fab --warn-only run_secure); exit ;;
        cookies_xss )       (cd cookies; fab --warn-only run_httponly); exit ;;

        clear_all_scores ) fab --warn-only clear_all_scores; exit ;;

        * ) echo "not a valid selection";
    esac
done
