#!/bin/bash 
apt-get -y install ruby1.9.1 ruby1.9.1-dev

gem install watir --no-rdoc --no-ri

apt-get -y install phantomjs

apt-get -y install xvfb
gem install headless