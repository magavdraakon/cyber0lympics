#!/usr/bin/env ruby
require 'rubygems'
require 'headless'
require 'watir'
require "watir-webdriver"

def datapath filename
  File.expand_path(File.join(File.dirname(__FILE__), filename))
end

def leave(message, code)
	puts message
	@browser.close
	@headless.destroy
	exit code
end

goto= ARGV[0]
filename="test.txt" #ARGV[1]
title='The banana way'
name='Benny Curved'
email='benny.curved@banania.ban'

@headless = Headless.new
@headless.start

@browser = Watir::Browser.new #:phantomjs # can not use phantom 
puts "Starting Gov contest functionality test"
@browser.goto(goto) #  URL in call
if @browser.link(:text =>'Contest').exists? then
	@browser.link(:text =>'Contest').click
	begin
		Watir::Wait.until{@browser.text.include? 'Art contest'}
	rescue
		leave("timeout! contest link doesnt work", 2)
	else # loaded 
		if @browser.text.include?('title missing') || @browser.text.include?('name missing') || @browser.text.include?('email missing') then
			leave("Validation errors on pageload -> static page?", 2)
		else # OK, no error messages on pageload, fill some fields
			@browser.text_field(:name => "title").set(title)
			@browser.file_field(:name => "entry").set(datapath(filename))
			@browser.element(:id=>'contest').click
			begin 
				Watir::Wait.until{@browser.element(:id=>'contest').value == 'Try again'}
			rescue
				leave("timeout! unexpected result on half filled form", 2)
			else # form reloaded, errors expected
				if @browser.text.include?('name missing') && @browser.text.include?('email missing') then
					# ok, see if fields refilled
					if @browser.text_field(:name => "title").value == title then
						#ok, fill all fields and submit
						@browser.file_field(:name => "entry").set(datapath(filename))
						@browser.text_field(:name => "name").set(name)
						@browser.text_field(:name => "email").set(email)
						@browser.element(:id=>'contest').click
						begin
							Watir::Wait.until{@browser.element(:id=>'contest').value == 'Submit'}
						rescue
							leave("timeout! unexpected page after submitting filled form", 2)
						else # page reloaded, check result
							if @browser.text.include?('Upload successful') then
								# ok
								leave("expected Gov contest functionality", 0)
							else # some error with submission
								leave("unexpected failure upon submitting", 2)
							end
						end
					else
						leave("form fields not refilled", 2) 
					end
				else
					leave("no error messages for half-filled form", 2)
				end
			end
		end
	end
else
	leave("Cant load page, is the link correct?", 2)
end
#@browser.screenshot.save "ww-phantomjs.png"
@headless.destroy
@browser.close
