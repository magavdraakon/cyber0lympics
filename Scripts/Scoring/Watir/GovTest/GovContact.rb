#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"

def leave(message, code)
	puts message
	@browser.close
	exit code
end

goto=ARGV[0]
topic='Praised be The banana way'
feedbk='TODO: some longer text'
name='Tru Bana'
email='tru.bana@banania.ban'

@browser = Watir::Browser.new :phantomjs 
puts "Starting Gov contact functionality test"
@browser.goto(goto)  #  URL in call
if @browser.link(:text =>'Contact us').exists? then
	@browser.link(:text =>'Contact us').click
	begin
		Watir::Wait.until{@browser.text.include? 'Contact us'}
	rescue
		leave("unexpected page for contact", 2)
	else
		if @browser.text.include?('Thank you!') || @browser.text.include?('topic missing') || @browser.text.include?('Please leave a few words') || @browser.text.include?('name missing') || @browser.text.include?('email missing')  then
			leave("validation notifications on pageload -> static page?", 2)
		else # OK, no thank you on pageload, fill some fields
			@browser.text_field(:name => "topic").set(topic)
			@browser.textarea(:name => "feedbk").set(feedbk)
			@browser.element(:id=>'contact').click
			begin 
				Watir::Wait.until{@browser.element(:id=>'contact').value == 'Try again'}
			rescue
				leave("timeout! unexpected page for half filled form", 2)
			else # loaded nicely, errors expected
				if @browser.text.include?('name missing') && @browser.text.include?('email missing') then
					if @browser.text_field(:name => "topic").value == topic &&  @browser.textarea(:name => "feedbk").value == feedbk then
						@browser.text_field(:name => "name").set(name)
						@browser.text_field(:name => "email").set(email)
						@browser.element(:id=>'contact').click
						begin 
							Watir::Wait.until{@browser.element(:id=>'contact').value == 'Send'}
						rescue
							leave("timeout! unexpected result for filled form", 2)
						else #loaded, no errors expected
							if @browser.text.include?('Thank you!') then
								leave("expected Gov contact functionality", 0)
							else
								leave("some problem with filled form", 2)
							end
						end
					else
						leave("fields are not refilled", 2)
					end
				else 
					leave("No error messages for half-filled form", 2)
				end
			end
		end
	end
else
	leave("Cannot find link, is it the correct URL?", 2)
end
#@browser.screenshot.save "ww-phantomjs.png"
@browser.close
