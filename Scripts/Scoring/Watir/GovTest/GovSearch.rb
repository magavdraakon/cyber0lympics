#!/usr/bin/env ruby
require 'rubygems'
require 'headless'
require 'watir'
require "watir-webdriver"

def leave(message, code)
	puts message
	@browser.close
	@headless.destroy
	exit code
end

goto= ARGV[0]
search1= 'population'
search2= 'Population'
result2= 'Population: 1.6 mil Bananians'

@headless = Headless.new
@headless.start

@browser = Watir::Browser.new #:phantomjs
puts "Starting Gov search functionality test"
@browser.goto(goto) #  URL in call
if @browser.text_field(:name => "q").exists? then
	@browser.text_field(:name => "q").set(search1)
	@browser.send_keys :enter
	begin
		Watir::Wait.until{@browser.text.include? 'Showing results for: "'+search1+'"'}
	rescue
		leave("unexpected result page for search", 2)
	else
		if @browser.element(:id=>'population').exists? then 
			# try other keyword (wont test field existance???)
			@browser.text_field(:name => "q").set(search2)
			@browser.send_keys :enter
			begin
				Watir::Wait.until{@browser.text.include? 'Showing results for: "'+search2+'"'}
			rescue
				leave("unexpected result page for second search", 2)
			else
				if @browser.text.include?(result2) then
					leave("expected Gov search results", 0)
				else
					leave("second search failed", 2)
				end
			end
		else 
			leave("first search failed", 2)
		end
	end
else
	leave("cant find field, is the url correct?", 2)
end
#@browser.screenshot.save "ww-phantomjs.png"
@headless.destroy
@browser.close
