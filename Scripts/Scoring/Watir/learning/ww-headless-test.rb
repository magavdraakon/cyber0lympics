require 'rubygems'
require 'headless'
require 'watir'
require "watir-webdriver"

headless = Headless.new
headless.start

browser = Watir::Browser.new
browser.goto "http://robot.itcollege.ee"
browser.text_field(:name => "s").set 'NEVE'
browser.send_keys :enter
sleep 5 # needed for page reload
headless.take_screenshot "headless.png"
headless.destroy
