# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep
import sys
import io, json

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = True
fabric.state.output.warn = True
env.user = 'root'
env.govtaskname = 'govX.ban'
env.shoptaskname = 'shopX.ban'

result, kali, hosting, fw = {}, {}, {}, {}

env.roledefs = {
    'scoring': ['scoreboard.ban', 'robot.itcollege.ee'],
    'red': ['192.168.100.104'],
    'green': ['192.168.100.101','192.168.100.102','192.168.100.103'],
    'green_gov': ['192.168.100.102'],
    'green_shop': ['192.168.100.103'],
    'kali': [],
    'hosting': [],
    'fw': [] }

def write_json(task_prefix='NONE'):
    print("Writing json file")
    with io.open(task_prefix+'.json','w',encoding='utf-8') as of:
        of.write('{')
        for i in range(1, 27):
            try:
                fh = open('results/'+task_prefix+str(i)+'.json')
            except IOError, e:
                print("IOError: results/%s%s.json doesn't exist" % (task_prefix, str(i)))
                continue
            line = fh.readline()
            if i < 26:
                of.write(line[1:-1]+',\n')
            else:
                of.write(line[1:-1]+'\n')
            fh.close()
        of.write('}')

@roles('scoring')
def update_scoring(task_prefix='taskKali'):
    #task_prefix='taskKali'
    print("updating scoring for", env.host)
    with cd('/var/cyber0lympics/ui/json/'):
        put(task_prefix+'.json', task_prefix+'.json')

def read_result(filename, i):
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            if line:
                prev = json.loads(line, encoding='utf-8')
                prev_up = int(prev[str(i)]['up'])
                prev_down = int(prev[str(i)]['down'])
            else:
                prev_down, prev_up = (0, 0)
            fh.close()
    except IOError, e:
        prev_down, prev_up = (0, 0)

    return (prev_down, prev_up)

def write_result(i, result, filename):
    return_code = result if isinstance( result, ( int, long ) ) else result.return_code
    if return_code == 0:
        print("target_"+str(i), ":OK", " Team: ", i)
        status = 'green'
    else:
        print("Return code", return_code, result)
        status = 'red'

    prev_down, prev_up = read_result(filename, i)

    if status == 'green':
        prev_up += 1
    elif status == 'red':
        prev_down += 1

    data = {i: {'status': status, 'up': str(prev_up),
                                'down': str(prev_down)}}

    print("Host: target%d, Status: %s, up: %d, down: %d" % (i, status, prev_up, prev_down))

    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))

###################################################################################
##                             TEST GOV                                          ##
##                                                                               ##
@roles('green_gov')
@parallel
def do_test_gov():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        for i in range(1, 27):
            filename = "results/" + env.govtaskname + str(i) + ".json"
            try:
                up = run("/root/cyber0lympics/Scripts/Scoring/general/grepUrlString.sh http://gov%d.ban/ anything" % i)
                if up.return_code < 2:
                    result = run("cd /root/cyber0lympics/Scripts/Scoring/Watir && ./GovFunctional.sh http://gov%d.ban/" % i)
                else:
                    result = 2
                write_result(i, result, filename)

            except Exception, e:
                print(e)
                write_result(i, 2, filename)

def run_gov():
    while True:
        execute(do_test_gov)
        sleep(1)
        write_json(env.govtaskname)
        execute(update_scoring, task_prefix=env.govtaskname)
        print("SLEEP 10 do_test_gov (safe to stop)")
        sleep(10)

###################################################################################
##                            TEST SHOP                                          ##
##                                                                               ##

@roles('green_shop')
@parallel
def do_test_shop():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        for i in range(1, 27):
            filename = "results/" + env.shoptaskname + str(i) + ".json"
            try:
                up = run("/root/cyber0lympics/Scripts/Scoring/general/grepUrlString.sh http://shop%d.ban/ Banania" % i)
                if up.return_code < 2:
                    result = run("cd /root/cyber0lympics/Scripts/Scoring/Watir && ./ShopFunctional.sh http://shop%d.ban/" % i)
                else:
                    result = 2
                write_result(i, result, filename)

            except Exception, e:
                print(e)
                write_result(i, 2, filename)

def run_shop():
    while True:
        execute(do_test_shop)
        sleep(1)
        write_json(env.shoptaskname)
        execute(update_scoring, task_prefix=env.shoptaskname)
        print("SLEEP 10 do_test_shop (safe to stop)")
        sleep(10)