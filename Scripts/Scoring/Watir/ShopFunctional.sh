#!/bin/bash 

TARGET=$1

# test php
ruby PhpSum.rb $TARGET
# if php works - register/login
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
ruby ShopTest/ShopRegLog.rb $TARGET

# if register/login works - basket
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
ruby ShopTest/ShopBasket.rb $TARGET

# if basket works - order
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
ruby ShopTest/ShopOrder.rb $TARGET

# if order works - admin
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
ruby ShopTest/ShopAdmin.rb $TARGET

# if admin works return code 0 == success
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
exit 0;