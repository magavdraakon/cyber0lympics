
# Watir

## completed scripts

### GovTest

tests, if a banana picture appears upon searching 'population' and the population info upon 'Population'

```ruby GovSearch.rb http://127.0.0.1/cyber0lympics/Targets/Gov/```

tests contact form 

```ruby GovContact.rb http://127.0.0.1/cyber0lympics/Targets/Gov/```

tests contest submission form

```ruby GovContest.rb http://127.0.0.1/cyber0lympics/Targets/Gov/ test.txt```

## Setup and examples

```
sudo apt-get install ruby1.9.1 ruby1.9.1-dev

```

# watir-webdriver

good documentation

* http://watirwebdriver.com
  * http://watirwebdriver.com/sending-special-keys/ 
* http://www.rubydoc.info/gems/watir-webdriver/
* https://leanpub.com/watirways/read
* https://github.com/watir/watir/wiki/Cheat-Sheet

** install **

```
sudo gem install watir --no-rdoc --no-ri
sudo gem install watir-webdriver --no-ri --no-rdoc # pole kindel kas vaja
```
** example **

```ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"
browser = Watir::Browser.new
browser.goto "http://robot.itcollege.ee"
browser.text_field(:name => "s").set 'NEVE'
browser.send_keys :enter
# browser.close
```

## headless

* http://watirwebdriver.com/headless/
* https://github.com/leonid-shevtsov/headless

PROBLEMS:

* makes screenshot of web browser with vertical scrollbar (some content hidden) see ** headless-saavutused.png **

** install **
```
sudo apt-get install xvfb
sudo gem install headless
sudo apt-get install imagemagick
```

** example **

```
require 'rubygems'
require 'headless'
require 'watir'
require "watir-webdriver"

headless = Headless.new
headless.start

browser = Watir::Browser.new
browser.goto "http://robot.itcollege.ee"
browser.text_field(:name => "s").set 'NEVE'
browser.send_keys :enter
sleep 5 # needed for page reload
headless.take_screenshot "headless.png"
headless.destroy
```

## phantomjs

Watir-webdriver and phantomJS

PROS:

* screenshot of whole page without scrollbar (more content = higher image) see ** phantomjs-saavutused.png **

** install **

```
sudo apt-get install phantomjs
```

```ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"
browser = Watir::Browser.new :phantomjs

browser.window.resize_to(1024, 768)

browser.goto "http://robot.itcollege.ee"
browser.text_field(:name => "s").set 'NEVE'
browser.send_keys :enter
sleep 5
browser.screenshot.save "ww-phantomjs.png"
```

# selenium-webdriver

** install **

```
sudo gem install selenium-webdriver --no-ri --no-rdoc
```

** example **

```ruby
require "selenium-webdriver"
browser = Selenium::WebDriver.for :firefox
browser.get "http://robot.itcollege.ee"
browser.find_element(name: "s").send_keys 'NEVE'
browser.find_element(name: "s").send_keys :enter
# browser.close
```

## PhantomJS

selenium webdriver + screenshots

PROS:

* screenshot of whole page without scrollbar (more content = higher image) see ** phantomjs-saavutused.png **

** install **

```
sudo apt-get install phantomjs
```

Useful links: 

* http://yizeng.me/2014/02/23/how-to-get-window-size-resize-or-maximize-window-using-selenium-webdriver/#ruby-example 

** example **

```ruby
require "selenium-webdriver"
browser = Selenium::WebDriver.for :phantomjs
browser.manage.window.resize_to(1024, 768)
browser.get "http://robot.itcollege.ee"
browser.find_element(name: "s").send_keys 'NEVE'
browser.find_element(name: "s").send_keys :enter
sleep 5 # needed for page reload
browser.save_screenshot "phantomjs.png"
```
