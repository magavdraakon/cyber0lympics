#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"

def leave(message, code)
	puts message
	@browser.close
	exit code
end

goto=ARGV[0]
username=(0...12).map { ('a'..'z').to_a[rand(26)] }.join # think of a username
password=(0...12).map { ('a'..'z').to_a[rand(26)] }.join # and a password
email='tru.bana@banania.ban' 
# REGISTER
@uError='please specify an username'
@pError='please fill in both passwords'
@eError='please specify an email address'
@oError='the username is already in use'

@loadSuccess='Register to buy'
@regSuccess='Successful register, you have been logged in'

# LOGIN
@luError='Please fill in your login information'
@lwError='unable to log in with that info'

@lloadSuccess='Log in to order'
@logSuccess='Successful login'

def register(username, password, email)
	if @browser.link(:text =>'Register').exists? then
		@browser.link(:text =>'Register').click
		begin
			Watir::Wait.until{@browser.text.include?(@loadSuccess)}
		rescue
			leave("timeout! register link doesnt work",2)
		else 
			if @browser.text.include?(@uError) || @browser.text.include?(@eError) || @browser.text.include?(@pError) then
				leave("validation errors on pageload. Is this a static page?", 2)
			else
				# pageload without errors, fill some fields
				@browser.text_field(:name => "username").set(username)
				@browser.text_field(:name => "email").set(email) 
				@browser.element(:id=>'register').click
				begin 
					Watir::Wait.until{@browser.element(:id=>'register').value == 'Try again'}
				rescue
					leave("timeout! form is not shown again after filling it half-way", 2)
				else  # no timeout,  check for error
					if @browser.text.include?(@pError) then # check re-filling
						if @browser.text_field(:name => "username").value == username && @browser.text_field(:name => "email").value == email then
							#continue filling
							@browser.text_field(:name => "password").set(password)
							@browser.text_field(:name => "password2").set(password) 
							@browser.element(:id=>'register').click
							begin
								Watir::Wait.until{@browser.text.include?(@regSuccess)}
							rescue
								leave("timeout! unexpected page after register", 2)
							else
								if @browser.link(:text =>username).exists? then
									return true
								else
									leave("functionality ok, but wrong username in header", 2)
								end
							end
						else
							leave( "half-filled form is not refilled after submit", 2)
						end

					else
						leave("there are no errors upon submitting an half-filled form",2)
					end
				end
			end
		end
	else
		leave("cant load page. Is the address correct?", 2)
	end
end

def login(username, password)
	if @browser.link(:text =>'Login').exists? then
		@browser.link(:text =>'Login').click
		begin
			Watir::Wait.until{@browser.text.include?(@lloadSuccess)}
		rescue
			leave("timeout! login link doesnt work",2)
		else 
			if @browser.text.include?(@luError) || @browser.text.include?(@lwError) then
				leave("validation errors on pageload. Is this a static page?", 2)
			else
				# pageload without errors, fill some fields
				@browser.text_field(:name => "username").set(username)
				@browser.element(:id=>'login').click
				begin 
					Watir::Wait.until{@browser.element(:id=>'login').value == 'Try again'}
				rescue
					leave("timeout! form is not shown again after filling it half-way", 2)
				else  # no timeout,  check for error
					if @browser.text.include?(@luError) then # check re-filling
						if @browser.text_field(:name => "username").value == username then
							#continue filling with wrong info
							@browser.text_field(:name => "password").set(password+"wrong")
							@browser.element(:id=>'login').click

							begin
								Watir::Wait.until{@browser.text_field(:name => "password").value == ''}
							rescue
								leave("timeout! wont ask again with wrong password", 2)
							else # saw error, fill it correctly
								if (@browser.text.include?(@lwError)) then 
									@browser.text_field(:name => "password").set(password)
									@browser.element(:id=>'login').click
									begin
										Watir::Wait.until{@browser.text.include?(@logSuccess)}
									rescue
										leave("timeout! unexpected page after login. Wrong info?", 2)
									else
										if @browser.link(:text =>username).exists? then
											return true
										else
											leave("functionality ok, but wrong username in header", 2)
										end
									end
								else
									leave("no error message with wrong password", 2)
								end
							end
						else
							leave( "half-filled form is not refilled after submit", 2)
						end

					else
						leave("there are no errors upon submitting an half-filled form",2)
					end
				end
			end
		end
	else
		leave("cant load page. Is the address correct?", 2)
	end
end

def logout(username)
	message='Welcome!'
	if @browser.link(:text =>'Logout').exists? then
		@browser.link(:text =>'Logout').click
		begin
			Watir::Wait.until{@browser.text.include?(message)}
		rescue
			return false
		else #logged out
			if @browser.link(:text =>username).exists? then
				return false 
			else
				return true # really did log out
			end
		end
	end
end


@browser = Watir::Browser.new :phantomjs
puts "Starting Reg/log functionality test" 
@browser.goto(goto)  #  URL in call
if register(username, password, email) then
	if logout(username) then
		if login(username, password) then
			leave("Reg/log successful", 0)
		else
			leave("Reg/log failed at login", 2)
		end
	end
end
@browser.close