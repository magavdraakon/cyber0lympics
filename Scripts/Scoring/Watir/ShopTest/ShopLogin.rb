#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"

def leave(message, code)
	puts message
	@browser.close
	exit code
end

goto=ARGV[0]
username='BANANA1'
password='TruBana'

uError='Please fill in your login information'
wError='unable to log in with that info'

loadSuccess='Log in to order'
logSuccess='Successful login'


@browser = Watir::Browser.new #:phantomjs 

@browser.goto(goto)  #  URL in call
if @browser.link(:text =>'Login').exists? then
	@browser.link(:text =>'Login').click
	begin
		Watir::Wait.until{@browser.text.include?(loadSuccess)}
	rescue
		leave("timeout! login link doesnt work",2)
	else 
		if @browser.text.include?(uError) || @browser.text.include?(wError) then
			leave("validation errors on pageload. Is this a static page?", 2)
		else
			# pageload without errors, fill some fields
			@browser.text_field(:name => "username").set(username)
			@browser.element(:id=>'login').click
			begin 
				Watir::Wait.until{@browser.element(:id=>'login').value == 'Try again'}
			rescue
				leave("timeout! form is not shown again after filling it half-way", 2)
			else  # no timeout,  check for error
				if @browser.text.include?(uError) then # check re-filling
					if @browser.text_field(:name => "username").value == username then
						#continue filling with wrong info
						@browser.text_field(:name => "password").set(password+"wrong")
						@browser.element(:id=>'login').click

						begin
							Watir::Wait.until{@browser.text_field(:name => "password").value == ''}
						rescue
							leave("timeout! wont ask again with wrong password", 2)
						else # saw error, fill it correctly
							if (@browser.text.include?(wError)) then 
								@browser.text_field(:name => "password").set(password)
								@browser.element(:id=>'login').click
								begin
									Watir::Wait.until{@browser.text.include?(logSuccess)}
								rescue
									leave("timeout! unexpected page after login. Wrong info?", 2)
								else
									if @browser.link(:text =>username).exists? then
										leave( "page loaded as expected", 0)
									else
										leave("functionality ok, but wrong username in header", 2)
									end
								end
							else
								leave("no error message with wrong password", 2)
							end
						end
					else
						leave( "half-filled form is not refilled after submit", 2)
					end

				else
					leave("there are no errors upon submitting an half-filled form",2)
				end
			end
		end
	end
else
	leave("cant load page. Is the address correct?", 2)
end
@browser.close