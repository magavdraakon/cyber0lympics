#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"

def leave(message, code)
	puts message
	@browser.close
	exit code
end

goto=ARGV[0]
username='BANANA2'
password='TruBana'
email='tru.bana@banania.ban'

uError='please specify an username'
pError='please fill in both passwords'
eError='please specify an email address'

loadSuccess='Register to buy'
regSuccess='Successful register, you have been logged in'

@browser = Watir::Browser.new #:phantomjs 

@browser.goto(goto)  #  URL in call
if @browser.link(:text =>'Register').exists? then
	@browser.link(:text =>'Register').click
	begin
		Watir::Wait.until{@browser.text.include?(loadSuccess)}
	rescue
		leave("timeout! register link doesnt work",2)
	else 
		if @browser.text.include?(uError) || @browser.text.include?(eError) || @browser.text.include?(pError) then
			leave("validation errors on pageload. Is this a static page?", 2)
		else
			# pageload without errors, fill some fields
			@browser.text_field(:name => "username").set(username)
			@browser.text_field(:name => "email").set(email) 
			@browser.element(:id=>'register').click
			begin 
				Watir::Wait.until{@browser.element(:id=>'register').value == 'Try again'}
			rescue
				leave("timeout! form is not shown again after filling it half-way", 2)
			else  # no timeout,  check for error
				if @browser.text.include?(pError) then # check re-filling
					if @browser.text_field(:name => "username").value == username && @browser.text_field(:name => "email").value == email then
						#continue filling
						@browser.text_field(:name => "password").set(password)
						@browser.text_field(:name => "password2").set(password) 
						@browser.element(:id=>'register').click
						begin
							Watir::Wait.until{@browser.text.include?(regSuccess)}
						rescue
							leave("timeout! unexpected page after register", 2)
						else
							if @browser.link(:text =>username).exists? then
								leave( "page loaded as expected", 0)
							else
								leave("functionality ok, but wrong username in header", 2)
							end
						end
					else
						leave( "half-filled form is not refilled after submit", 2)
					end

				else
					leave("there are no errors upon submitting an half-filled form",2)
				end
			end
		end
	end
else
	leave("cant load page. Is the address correct?", 2)
end
@browser.close