#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"
# a test to see if ordering itself works 
# pre-requisite is that the basket and login test succeeds

@goto=ARGV[0]
username='test'
password='testPW'
itemIndex= rand(11)+1
howMany= rand(6)+1
noAddress="Please specify a billing address"
noItems="Order can't be empty"
addressIs="New Banania 24-7\nBanania"
oSuccess="Ordering successful"

def leave(message, code)
	puts message
	@browser.close
	exit code
end

def login(username, password)
	loadSuccess='Log in to order'
	logSuccess='Successful login'
	if @browser.link(:text =>'Login').exists? then
		@browser.link(:text =>'Login').click
		begin
			Watir::Wait.until{@browser.text.include?(loadSuccess)}
		rescue
			leave("timeout! login link doesnt work",2)
		else 
			@browser.text_field(:name => "username").set(username)
			@browser.text_field(:name => "password").set(password)
			@browser.element(:id=>'login').click
			begin
				Watir::Wait.until{@browser.text.include?(logSuccess)}
			rescue
				leave("timeout! unexpected page after login. Wrong info?", 2)
			else
				if @browser.link(:text =>username).exists? then
					return true
				else
					return false
				end
			end
		end
	end
end

def logout(username)
	message='Welcome!'
	if @browser.link(:text =>'Logout').exists? then
		@browser.link(:text =>'Logout').click
		begin
			Watir::Wait.until{@browser.text.include?(message)}
		rescue
			return false
		else #logged out
			if @browser.link(:text =>username).exists? then
				return false 
			else
				return true # really did log out
			end
		end
	end
end


def click_button(index, action, view)
	tr=get_tr(index)
	if tr then
		basket=in_basket(index, view)
		if tr.link(:class=>action).exists? then
			tr.link(:class=>action).click
			tr = get_tr(index)
			nbasket=in_basket(index, view)
			if (nbasket-basket).abs==1 then # adding to basket works
				return true
			else
				puts "product amount in basket has not de/increased"
				return false
			end
		else
			puts "no such link #{index} #{action}"
			return false
		end
	else
		return false
	end
end

def get_tr(index)
	if @browser.trs.length-1<index then
		puts "no such index! max: "+(@browser.trs.length-1).to_s
		return false
	else
		if @browser.trs[index].exists? then
			return @browser.trs[index]
		else
			puts "tr doesnt exist"
			return false
		end
	end
end

def in_basket(index, view)
	tr=get_tr(index)
	if tr then
		if view=="shop" then
			return tr.tds[4].text.to_i
		else
			return tr.tds[3].text.to_i
		end
	else
		return 0
	end
end


@browser = Watir::Browser.new :phantomjs 
puts "starting Ordering functionality test (#{howMany} of item #{itemIndex})"
@browser.goto(@goto) 

if login(username, password) then
	# go to shop
	if @browser.link(:text =>'Shop').exists? then
		@browser.link(:text =>'Shop').click
		begin 
			Watir::Wait.until{@browser.text.include?("Stock")}
		rescue
			leave("timeout! shop page not displayed", 2)
		else # ok, add some item
			for i in 1..howMany
				status=click_button(itemIndex, "add", "shop")
				leave("Adding item or increasing it's amount failed", 2) if !status
			end
			#go to basket
			if @browser.link(:text =>'Basket').exists? then
				@browser.link(:text =>'Basket').click
				begin 
					Watir::Wait.until{@browser.link(:text=>"Confirm order").exists? }
				rescue
					leave("timeout! basket page not displayed after adding some items", 2)
				else 
					# confirm
					@browser.link(:text=>"Confirm order").click
					begin 
						Watir::Wait.until{@browser.textarea(:name=>"billing").exists? }
					rescue
						leave("cant see order confirmation", 2)
					else
						#check for errors on pageload
						if @browser.text.include?(noItems) || @browser.text.include?(noAddress) then
							leave("validation errors on order pageload", 2)
						else #ok, no errors now uncheck all
							@browser.checkboxes.each do |c|
								c.set(false)
							end
							@browser.textarea(:name=>"billing").set('')
							@browser.element(:id=>'order').click
							begin 
								Watir::Wait.until{ @browser.text.include?(noItems) && @browser.text.include?(noAddress)}
							rescue
								leave('no errrors with empty form', 2)
							else				
								#fill correctly
								@browser.checkboxes.each do |c|
									c.set(true)
								end
								@browser.textarea(:name=>"billing").set(addressIs)
								@browser.element(:id=>'order').click	
								begin
									Watir::Wait.until{ @browser.text.include?(oSuccess)}
								rescue
									leave('No success notification', 2)
								else
									# reload page to check if message gowes away
									@browser.refresh
									begin
										Watir::Wait.until{ !@browser.text.include?(oSuccess)}
									rescue
										leave('Success notification doesnt go away. static page?', 2)
									else
										leave("expected Ordering functionality", 0)
									end	
								end			
							end
						end
					end
				end
			else
				leave('cant find basket link', 2)
			end
		end
	else
		leave('cant find shop link', 2)
	end	
else
	leave("can't log in", 2)
end
