#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"
# a test to see if simple users are limited and admins empowered
# pre-requisites: login must work

@goto=ARGV[0]
username='test'
password='testPW'
adminname='admin'
adminpw='adminPW'

def leave(message, code)
	puts message
	@browser.close
	exit code
end

def login(username, password)
	loadSuccess='Log in to order'
	logSuccess='Successful login'
	if @browser.link(:text =>'Login').exists? then
		@browser.link(:text =>'Login').click
		begin
			Watir::Wait.until{@browser.text.include?(loadSuccess)}
		rescue
			leave("timeout! login link doesnt work",2)
		else 
			@browser.text_field(:name => "username").set(username)
			@browser.text_field(:name => "password").set(password)
			@browser.element(:id=>'login').click
			begin
				Watir::Wait.until{@browser.text.include?(logSuccess)}
			rescue
				leave("timeout! unexpected page after login. Wrong info?", 2)
			else
				if @browser.link(:text =>username).exists? then
					return true
				else
					return false
				end
			end
		end
	end
end

def logout(username)
	message='Welcome!'
	if @browser.link(:text =>'Logout').exists? then
		@browser.link(:text =>'Logout').click
		begin
			Watir::Wait.until{@browser.text.include?(message)}
		rescue
			return false
		else #logged out
			if @browser.link(:text =>username).exists? then
				return false 
			else
				return true # really did log out
			end
		end
	end
end

def adminView(link)
	@browser.goto(@goto)
	if link then
		#try to use link in menu
		if @browser.link(:text =>'View all orders').exists? then
			@browser.link(:text =>'View all orders').click
		else
			#puts "no link"
			return false # no link where it should be
		end
	else
		#go to url,  add ?page=all_orders to the end after last /
		adminView=@goto.rpartition('/').first+'/?page=all_orders'
		@browser.goto(adminView)
	end
	# check if on page not depending on way of departure
	if @browser.text.include?('Displaying all orders') then
		return true # displaying page without error message
	else # no title, has error message..or some other situation
		return false
	end
end

@browser = Watir::Browser.new :phantomjs 
puts "starting admin role test"
@browser.goto(@goto)

#test if admin view accessible for unlogged user - expect false
if adminView(false)==false then
	#ok without link, what about link?
	if adminView(true)==false then
		#ok, now log as simple user and try again
		if login(username, password) then# simple user in
			#test if admin view accessible - expect false
			if adminView(false)==false then
				#ok, what about the link?
				if adminView(true)==false then
					#ok, log as admin instead
					if logout(username) then # simple user out
						if login(adminname, adminpw) then #admin in
							#test if admin view accessible - expect true
							if adminView(false)==true then
								#ok, try link
								if adminView(true)==true then
									leave('expected role functionality', 0)
								else
									leave('admin didnt get to the admin view w link', 2)
								end
							else
								leave('admin didnt get to the admin view w/o link', 2)
							end
						else
							leave("admin login failed", 2)
						end
					else
						leave("Can't log out simpleuser", 2)
					end
				else
					leave('user got to the admin view w link - ',2)
				end
			else
				leave('user got to the admin view w/o link - ',2)
			end
		else
			leave("simple-user login failed", 2)
		end
	else
		leave('anonymous got to the admin view w link - ', 2)
	end
else
	leave('anonymous got to the admin view w/o link',2)
end

@browser.close