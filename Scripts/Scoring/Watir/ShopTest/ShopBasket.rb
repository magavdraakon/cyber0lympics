#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"
# a test to see if adding items to basket and removing them works without logging in

@goto=ARGV[0]
itemIndex= rand(11)+1
howMany= rand(6)+1

def leave(message, code)
	puts message
	@browser.close
	exit code
end

def click_button(index, action, view)
	tr=get_tr(index)
	if tr then
		basket=in_basket(index, view)
		if tr.link(:class=>action).exists? then
			tr.link(:class=>action).click
			tr = get_tr(index)
			nbasket=in_basket(index, view)
			if (nbasket-basket).abs==1 then # adding to basket works
				return true
			else
				puts "product amount in basket has not de/increased"
				return false
			end
		else
			puts "no such link: #{index} '#{action}'"
			return false
		end
	else
		return false
	end
end

def get_tr(index)
	if @browser.trs.length-1<index then
		puts "no such index! max: "+(@browser.trs.length-1).to_s
		return false
	else
		if @browser.trs[index].exists? then
			return @browser.trs[index]
		else
			puts "tr doesnt exist"
			return false
		end
	end
end

def in_basket(index, view)
	tr=get_tr(index)
	if tr then
		if view=="shop" then
			return tr.tds[4].text.to_i
		else
			return tr.tds[3].text.to_i
		end
	else
		return 0
	end
end

@browser = Watir::Browser.new :phantomjs 
puts "starting Basket functionality test (#{howMany} of item #{itemIndex})"
@browser.goto(@goto)
# see if basket empty
if @browser.link(:text =>'Basket').exists? then
	@browser.link(:text =>'Basket').click
	begin 
		Watir::Wait.until{@browser.text.include?("Go to the Shop to add items to your basket")}
	rescue
		leave("timeout! empty basket page not displayed", 2)
	else #ok, go to shop 
		if @browser.link(:text =>'Shop').exists? then
			@browser.link(:text =>'Shop').click
			begin 
				Watir::Wait.until{@browser.text.include?("Stock")}
			rescue
				leave("timeout! shop page not displayed", 2)
			else # ok, add one item	
				status=true
				for i in 1..(howMany+1)
					status=click_button(itemIndex, "add", "shop")
					leave("Adding item or increasing it's amount failed", 2) if !status
				end
				if click_button(itemIndex, "remove", "shop") then # go to basket and check status+empty
					if @browser.link(:text =>'Basket').exists? then
						@browser.link(:text =>'Basket').click
						begin 
							Watir::Wait.until{@browser.text.include?("To confirm your order you need to login OR register")}
						rescue
							leave("timeout! basket page not displayed after adding some items", 2)
						else
							# check amount of first item (only item)
							if @browser.trs.length>1 && in_basket(1, 'basket')==howMany then
								if @browser.link(:text =>'Empty basket').exists? then
									@browser.link(:text =>'Empty basket').click
									begin 
										Watir::Wait.until{@browser.text.include?("Go to the Shop to add items to your basket")}
									rescue
										leave("timeout! empty basket page not displayed after emptying basket", 2)
									else	
										leave("expected basket functionality", 0)
									end
								else
									leave("Cant find empty basket link", 2)
								end
							else
								leave("wrong amount of items in basket", 2)
							end
						end
					else
						leave("cant find basket link after adding items to basket", 2)
					end
				else
					leave('removing item from basket failed', 2)
				end
			end
		else
			leave('cant find shop link', 2)
		end
	end
else
	leave("cant find basket link", 2)
end