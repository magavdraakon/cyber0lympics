#!/bin/bash 

TARGET=$1

# test php
ruby PhpSum.rb $TARGET
# if php works - search
#rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
#ruby GovTest/GovSearch.rb $TARGET

# if search works - contact
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
ruby GovTest/GovContact.rb $TARGET

# if contact works - contest
#rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
#ruby GovTest/GovContest.rb $TARGET

# if contact works return code 0 == success
rc=$?; if [[ $rc == 2 ]]; then exit $rc; fi
exit 0;