#!/usr/bin/env ruby
require 'rubygems'
require 'watir'
require "watir-webdriver"

def leave(message, code)
	puts message
	@browser.close
	exit code
end

got= ARGV[0]
a= rand(100)+1 #(ARGV[1]).to_i # just take random
b= rand(100)+1 #(ARGV[2]).to_i # just take random
goto= got+(got.include?("page=")? '&': '?')+"a="+a.to_s+"&b="+b.to_s
sum = a+b
@browser = Watir::Browser.new :phantomjs
puts "Starting php test: #{a}+#{b}=#{sum}"
@browser.goto(goto) #  URL in call
begin
	Watir::Wait.until{@browser.element(:id=>'something').exists?}
rescue
	# no element -> no php
	leave("No connection, No element, No php or wrong url", 2)
else # element found
	# value?
	if @browser.element(:id=>'something').text.to_i == sum then
		#correct -> php works
		leave("PHP test successful", 0)
	else
		# wrong value
		leave("wrong value. expected #{sum} Got "+@browser.element(:id=>'something').text, 2)
	end
end
#browser.screenshot.save "ww-phantomjs.png"
@browser.close
