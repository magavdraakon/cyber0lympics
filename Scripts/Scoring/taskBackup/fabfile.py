# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep

import io, json


# fab -i ~/.ssh/id_rsa_scoring --skip-bad-hosts -P  --warn-only is_backup  --hide running

# Uus ühendus
# fab --warn-only is_backup

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = False
fabric.state.output.warn = False
env.user = 'root'

result = dict()
env.roledefs = {'scoring': ['scoreboard.ban', 'robot.itcollege.ee']}

for i in range(1, 41):
    hostname = "10.1.%s.66" % str(i + 10)
    env.hosts.append("10.1.%s.66" % str(i + 10))
    result[hostname] = i

print("before: %s" % fabric.state.output)


@parallel
def is_backup():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        try:
            a = run('[ -d /backup ] && [[ $(find /backup -size +10k | wc -l)  > 0 ]] || exit 2')
            if a.return_code == 0:
                print(env.host, ":OK", " Team: ", result[env.host])
                status = 'green' #Green
            elif a.return_code == 2:
                print(env.host, ":NOK")
                status = 'yellow' #
            else:
                print("Return code", a.return_code)
                status = 'red'


        except fabric.exceptions.NetworkError, ne:
            print("Error connecting to ", env.host)
            status = 'red'
        except Exception, e:
            print("Connected to ", env.host)
            print("Jama majas", type(e), e)
            """
            	"1": {"status": "warning",
                    "up": 0,
                    "wounded": 2,
                    "down": 0
                    },
            """

    filename = "results/taskBackup" + str(result[env.host]) + ".json"
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            prev = json.loads(line, encoding='utf-8')

            prev_up = int(prev[str(result[env.host])]['up'])
            prev_wounded = int(prev[str(result[env.host])]['wounded'])
            prev_down = int(prev[str(result[env.host])]['down'])
            print('Host:', env.host, 'previous: up', prev_up, 'previous: wounded', prev_wounded, 'previous: down', prev_down)
            fh.close()
    except Exception, e:
        prev_up, prev_wounded, prev_down = 0, 0, 0
    if status == 'green':
        prev_up += 1
    elif status == 'yellow':
        prev_wounded += 1
    elif status == 'red':
        prev_down += 1

    data = {result[env.host]:
                dict(status=status, up=str(prev_up), wounded=str(prev_wounded), down=str(prev_down))}


    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))


def write_json():
    print("Writing json file")
    with io.open('taskBackup.json','w',encoding='utf-8') as of:
        of.write('{')
        for i in range(1,41):
            fh = open('results/taskBackup'+str(i)+'.json')
            line = fh.readline()
            if i < 40:
                of.write(line[1:-1]+',\n')
            else:
                of.write(line[1:-1]+'\n')
            fh.close()
        of.write('}')


@roles('scoring')
def update_scoring():
    print("updating scoring")
    with cd('/var/cyber0lympics/ui/json/'):
        put('taskBackup.json', 'taskBackup.json')

#fab --warn-only run_backup
def run_backup():
    while True:
        execute(is_backup)
        sleep(1)
        write_json()
        execute(update_scoring)
        print("sleeping 10 seconds")
        sleep(10)