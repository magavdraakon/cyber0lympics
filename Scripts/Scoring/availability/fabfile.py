# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep
import sys
import io, json


# fab -i ~/.ssh/id_rsa_scoring --skip-bad-hosts -P  --warn-only is_backup  --hide running

# Uus ühendus
# fab --warn-only is_backup

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = False
fabric.state.output.warn = False
env.user = 'root'

result, kali, hosting, fw = {}, {}, {}, {}

env.roledefs = {'scoring': ['scoreboard.ban', 'robot.itcollege.ee'], 'red': ['192.168.100.101','192.168.100.102','192.168.100.103'], 'kali': [], 'hosting': [], 'fw': [] }

for i in range(1, 41):
    hostname = "10.1.%s.66" % str(i + 10)
    env.roledefs['kali'].append("10.1.%s.66" % str(i + 10))
    kali[hostname] = i

for i in range(1, 41):
    hostname = "10.1.%s.200" % str(i + 10)
    env.roledefs['hosting'].append("10.1.%s.200" % str(i + 10))
    hosting[hostname] = i

for i in range(1, 41):
    hostname = "10.2.%s.2" % str(i + 10)
    env.roledefs['fw'].append("10.2.%s.2" % str(i + 10))
    fw[hostname] = i

print("before: %s" % fabric.state.output)


@roles('red')
@parallel
def network():
    run('dhclient eth0')

@roles('red')
@parallel
def prepare():
    try:
        run('test -d /root/cyber0lympics || cd /root && git clone git@bitbucket.org:magavdraakon/cyber0lympics.git')
        with cd('/root/cyber0lympics/'):
            run('git pull')
            run('apt-get install curl -y')
            run('apt-get install apache2-utils -y')
            try:
                run('test -x /root/cyber0lympics/Scripts/Scoring/ddos/dos.sh')
            except Exception, e:
                print("Ilge seakisia")


    except fabric.exceptions.NetworkError, ne:
        print("Error connecting to ", env.host)
    except Exception, e:
        print("Connected to ", env.host)
        print("Jama majas", type(e), e)


@roles('kali')
@parallel
def kali_is_alive():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        try:
            a = run('uptime')
            if a.return_code == 0:
                # print(env.host, ":OK", " Team: ", kali[env.host])
                status = 'green'
            else:
                print("Return code", a.return_code)
                status = 'red'

        except fabric.exceptions.NetworkError, ne:
            print("Error connecting to ", env.host)
            status = 'red'
        except Exception, e:
            print("Connected to ", env.host)
            print("Jama majas", type(e), e)

    filename = "results/taskKali" + str(kali[env.host]) + ".json"
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            print(line)
            if line:
                prev = json.loads(line, encoding='utf-8')

                prev_up = int(prev[str(kali[env.host])]['up'])
                prev_down = int(prev[str(kali[env.host])]['down'])
                print('Host:', env.host, 'previous: up', prev_up, 'previous: down', prev_down)
            else:
                prev_down, prev_up = 0, 0
            fh.close()
    except IOError, e:
        prev_down, prev_up = 0, 0

    if status == 'green':
        prev_up += 1
    elif status == 'yellow':
        prev_down += 1
    elif status == 'red':
        prev_down += 1

    data = {kali[env.host]:
                dict(status=status, up=str(prev_up),  down=str(prev_down))}

    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))


def write_json(task_prefix='NONE'):
    print("Writing json file")
    with io.open(task_prefix+'.json','w',encoding='utf-8') as of:
        of.write('{')
        for i in range(1, 41):
            try:
                fh = open('results/'+task_prefix+str(i)+'.json')
                line = fh.readline()
                if i < 40:
                    of.write(line[1:-1]+',\n')
                else:
                    of.write(line[1:-1]+'\n')
                fh.close()
            except Exception, e:
                pass
        of.write('}')

@roles('hosting')
@parallel
def hosting_is_alive():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        try:
            a = local("curl --connect-timeout 5 -m 5 %s" % env.host)
            if a.return_code > 0:
                status = 'red'
            else:
                a = run('uptime')
                if a.return_code == 0:
                    print(env.host, ":OK", " Team: ", hosting[env.host])
                    status = 'green' #Green
                else:
                    print("Return code", a.return_code)
                    status = 'red'

        except fabric.exceptions.NetworkError, ne:
            print("Error connecting to ", env.host)
            status = 'red'
        except Exception, e:
            print("Connected to ", env.host)
            print("Jama majas", type(e), e)

    filename = "results/taskHosting" + str(hosting[env.host]) + ".json"
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            print(line)
            if line:
                prev = json.loads(line, encoding='utf-8')

                prev_up = int(prev[str(hosting[env.host])]['up'])
                prev_down = int(prev[str(hosting[env.host])]['down'])
                #print('Host:', env.host, 'previous: up', prev_up, 'previous: down', prev_down)
            else:
                prev_down, prev_up, = 0, 0
            fh.close()
    except IOError, e:
        prev_down, prev_up = 0, 0

    if status == 'green':
        prev_up += 1
    elif status == 'yellow':
        prev_down += 1
    elif status == 'red':
        prev_down += 1


    data = {hosting[env.host]:
                dict(status=status, up=str(prev_up),  down=str(prev_down))}

    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))


@roles('fw')
@parallel
def fw_is_alive():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        try:
            a = run('uptime')
            if a.return_code == 0:
                print(env.host, ":OK", " Team: ", fw[env.host])
                status = 'green' #Green
            else:
                print("Return code", a.return_code)
                status = 'red'

        except fabric.exceptions.NetworkError, ne:
            print("Error connecting to ", env.host)
            status = 'red'
        except Exception, e:
            print("Connected to ", env.host)
            print("Jama majas", type(e), e)

    filename = "results/taskFirewall" + str(fw[env.host]) + ".json"
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            print(line)
            if line:
                prev = json.loads(line, encoding='utf-8')

                prev_up = int(prev[str(fw[env.host])]['up'])
                prev_down = int(prev[str(fw[env.host])]['down'])
                print('Host:', env.host, 'previous: up', prev_up, 'previous: down', prev_down)
            else:
                prev_down, prev_up = 0, 0
            fh.close()
    except IOError, e:
        prev_down, prev_up = 0, 0

    if status == 'green':
        prev_up += 1
    elif status == 'yellow':
        prev_down += 1
    elif status == 'red':
        prev_down += 1

    data = {fw[env.host]: {'status': status, 'up': str(prev_up), 'down': str(prev_down)}}

    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))

@roles('scoring')
def update_scoring(task_prefix='taskKali'):
    #task_prefix='taskKali'
    print("updating scoring for", env.host)
    with cd('/var/cyber0lympics/ui/json/'):
        put(task_prefix+'.json', task_prefix+'.json')


def run_kali():
    while True:
        execute(kali_is_alive)
        sleep(1)
        write_json('taskKali')
        execute(update_scoring, task_prefix='taskKali')
        print("SLEEP 10 (kali)")
        sleep(10)


def run_hosting():
    while True:
        execute(hosting_is_alive)
        sleep(5)
        write_json('taskHosting')
        execute(update_scoring, task_prefix='taskHosting')
        print("SLEEP 10 (hosting)")
        sleep(10)


def run_fw():
    while True:
        execute(fw_is_alive)
        sleep(1)
        write_json('taskFirewall')
        execute(update_scoring, task_prefix='taskFirewall')
        print('SLEEP 10 (firewall)')
        sleep(10)