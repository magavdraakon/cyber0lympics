# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep
import sys
import io, json


# fab -i ~/.ssh/id_rsa_scoring --skip-bad-hosts -P  --warn-only is_backup  --hide running

# Uus ühendus
# fab --warn-only is_backup

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = True
fabric.state.output.warn = False
env.user = 'root'

result, kali, hosting, fw = {}, {}, {}, {}

env.roledefs = {'scoring': ['scoreboard.ban', 'robot.itcollege.ee'], 'red': ['192.168.100.101', '192.168.100.102', '192.168.100.103'], 'kali': [], 'hosting': [], 'fw': [] }

for i in range(1, 41):
    hostname = "10.1.%s.66" % str(i + 10)
    env.roledefs['kali'].append("10.1.%s.66" % str(i + 10))
    kali[hostname] = i

for i in range(1, 41):
    hostname = "10.1.%s.200" % str(i + 10)
    env.roledefs['hosting'].append("10.1.%s.200" % str(i + 10))
    hosting[hostname] = i

for i in range(1, 41):
    hostname = "10.2.%s.2" % str(i + 10)
    env.roledefs['fw'].append("10.2.%s.2" % str(i + 10))
    fw[hostname] = i

print("before: %s" % fabric.state.output)


@roles('red')
@parallel
def network():
    run('dhclient eth0')

@roles('red')
@parallel
def prepare():
    try:
        run('cd /root/cyber0lympics/ && git pull')


    except fabric.exceptions.NetworkError, ne:
        print("Error connecting to ", env.host)
    except Exception, e:
        print("Connected to ", env.host)
        print("Jama majas", type(e), e)


def write_json(task_prefix='NONE'):
    print("Writing json file")
    with io.open(task_prefix+'.json','w',encoding='utf-8') as of:
        of.write('{')
        for i in range(1, 41):
            try:
                fh = open('results/'+task_prefix+str(i)+'.json')
                line = fh.readline()
                if i < 40:
                    of.write(line[1:-1]+',\n')
                else:
                    of.write(line[1:-1]+'\n')
            except Exception, e:
                pass
            fh.close()
        of.write('}')


@roles('red')
# @parallel
def cookies_are_secure():

    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        for i in range(1, 41):
            try:
                # run("test -x /root/cyber0lympics/Scripts/Scoring/shellshock/ss_test.sh || cd /root/cyber0lympics/ && git pull")
                a = run("/root/cyber0lympics/Scripts/Scoring/cookies/cookie-test.py http://shop%s.ban/ secure" % i)
                print("VASTUS", a.return_code)
                if a.return_code == 0:
                    status = 'green'
                    print("Return code", a.return_code)
                elif a.return_code == 2:
                    status = 'yellow'
                else:
                    status = 'red'


            except fabric.exceptions.NetworkError, ne:
                print("Error connecting to terrorist")
            except Exception, e:
                print("Jama majas", type(e), e)

            filename = "results/taskCookieSecure" + str(i) + ".json"
            try:
                with io.open(filename) as fh:
                    line = fh.readline()
                    print(line)
                    if line:
                        prev = json.loads(line, encoding='utf-8')

                        prev_up = int(prev[str(i)]['up'])
                        prev_wounded = int(prev[str(i)]['wounded'])
                        prev_down = int(prev[str(i)]['down'])
                        #print('Host:', env.host, 'previous: up', prev_up, 'previous: down', prev_down)
                    else:
                        prev_down, prev_up, prev_wounded = 0, 0, 0
                    fh.close()
            except IOError, e:
                prev_down, prev_up, prev_wounded = 0, 0, 0

            if status == 'green':
                prev_up += 1
            elif status == 'yellow':
                prev_wounded += 1
            elif status == 'red':
                prev_down += 1

            data = {i:
                        dict(status=status, up=str(prev_up), wounded=str(prev_wounded), down=str(prev_down))}

            with io.open(filename, 'w', encoding='utf-8') as f:
                f.write(unicode(json.dumps(data, ensure_ascii=False)))

        write_json('taskCookieSecure')
        execute(update_scoring, task_prefix='taskCookieSecure')


@roles('red')
# @parallel
def cookies_are_httponly():

    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        for i in range(1, 41):
            try:
                # run("test -x /root/cyber0lympics/Scripts/Scoring/shellshock/ss_test.sh || cd /root/cyber0lympics/ && git pull")
                a = run("/root/cyber0lympics/Scripts/Scoring/cookies/cookie-test.py http://shop%s.ban/ HttpOnly" % i)
                print("VASTUS", a.return_code)
                if a.return_code == 0:
                    status = 'green'
                    print("Return code", a.return_code)
                elif a.return_code == 2:
                    status = 'yellow'
                else:
                    status = 'red'


            except fabric.exceptions.NetworkError, ne:
                print("Error connecting to terrorist")
            except Exception, e:
                print("Jama majas", type(e), e)

            filename = "results/taskHTTPCookie" + str(i) + ".json"
            try:
                with io.open(filename) as fh:
                    line = fh.readline()
                    print(line)
                    if line:
                        prev = json.loads(line, encoding='utf-8')

                        prev_up = int(prev[str(i)]['up'])
                        prev_wounded = int(prev[str(i)]['wounded'])
                        prev_down = int(prev[str(i)]['down'])
                        #print('Host:', env.host, 'previous: up', prev_up, 'previous: down', prev_down)
                    else:
                        prev_down, prev_up, prev_wounded = 0, 0, 0
                    fh.close()
            except IOError, e:
                prev_down, prev_up, prev_wounded = 0, 0, 0

            if status == 'green':
                prev_up += 1
            elif status == 'yellow':
                prev_wounded += 1
            elif status == 'red':
                prev_down += 1

            data = {i:
                        dict(status=status, up=str(prev_up), wounded=str(prev_wounded), down=str(prev_down))}

            with io.open(filename, 'w', encoding='utf-8') as f:
                f.write(unicode(json.dumps(data, ensure_ascii=False)))

        write_json('taskHTTPCookie')
        execute(update_scoring, task_prefix='taskHTTPCookie')



@roles('scoring')
def update_scoring(task_prefix='taskKali'):
    #task_prefix='taskKali'
    print("updating scoring for", env.host)
    with cd('/var/cyber0lympics/ui/json/'):
        put(task_prefix+'.json', task_prefix+'.json')


def run_httponly():
    print("excecuting TEST cookie HttpOnly")
    sleep(2)
    while True:
        execute(cookies_are_httponly)
        print('sleeping 10 seconds')
        sleep(10)

def run_secure():
    print("excecuting TEST cookie secure")
    sleep(2)
    while True:
        execute(cookies_are_secure)
        print('sleeping 10 seconds')
        sleep(10)