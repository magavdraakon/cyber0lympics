# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep
import sys
import io, json

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = True
fabric.state.output.warn = True
env.user = 'root'
env.scorefiles = [
    'govX.ban.json',
    'shopX.ban.json',
    'taskBackup.json',
    'taskCGI.json',
    'taskCommandExecution.json',
    'taskCookieSecure.json',
    'taskDDOS.json',
    'taskFirewall.json',
    'taskHTTPCookie.json',
    'taskHosting.json',
    'taskKali.json',
    'taskPathTraversal.json',
    'taskSQLi.json',
    'taskSSL.json',
    'taskXSS.json',
]

env.roledefs = {
    'scoring': ['scoreboard.ban', 'robot.itcollege.ee'],
    }

@roles('scoring')
def update_scoring(task_prefix='taskKali'):
    #task_prefix='taskKali'
    print("updating scoring for", env.host)
    with cd('/var/cyber0lympics/ui/json/'):
        put(task_prefix+'.json', task_prefix+'.json')

@roles('scoring')
@parallel
def clear_scoreboard_scores():
    with cd('/var/cyber0lympics/ui/json/'):
        for filename in env.scorefiles:
            put('empty_scores.json', filename)
            #run('rm /var/cyber0lympics/ui/json/'+filename)

def clear_all_scores():
    local("find . -path '*/results/*.json' -exec rm -rf {} \;")
    execute(clear_scoreboard_scores)
