# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep
import sys
import io, json

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = True
fabric.state.output.warn = True
env.user = 'root'
env.xsstaskanem = 'taskXSS'
env.pathtaskanem = 'taskPathTraversal'

result, kali, hosting, fw = {}, {}, {}, {}

env.roledefs = {
    'scoring': ['scoreboard.ban', 'robot.itcollege.ee'],
    'red': ['192.168.100.101','192.168.100.102','192.168.100.103'],
    'green': ['192.168.100.101','192.168.100.102','192.168.100.103'],
    'kali': [],
    'hosting': [],
    'fw': [] }

def write_json(task_prefix='NONE'):
    print("Writing json file")
    with io.open(task_prefix+'.json','w',encoding='utf-8') as of:
        of.write('{')
        for i in range(1, 41):
            try:
                fh = open('results/'+task_prefix+str(i)+'.json')
            except IOError, e:
                print("IOError: results/%s%s.json doesn't exist" % (task_prefix, str(i)))
                continue
            line = fh.readline()
            if i < 40:
                of.write(line[1:-1]+',\n')
            else:
                of.write(line[1:-1]+'\n')
            fh.close()
        of.write('}')

@roles('scoring')
def update_scoring(task_prefix='taskKali'):
    #task_prefix='taskKali'
    print("updating scoring for", env.host)
    with cd('/var/cyber0lympics/ui/json/'):
        put(task_prefix+'.json', task_prefix+'.json')


def read_result(filename, i):
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            if line:
                prev = json.loads(line, encoding='utf-8')
                prev_up = int(prev[str(i)]['up'])
                prev_wounded = int(prev[str(i)]['wounded'])
                prev_down = int(prev[str(i)]['down'])
                # print('Host:', "target_"+str(i), 'previous: up', prev_up, 'previous: wounded', prev_wounded, 'previous: down', prev_down)
            else:
                prev_down, prev_up, prev_wounded = (0, 0, 0)
            fh.close()
    except IOError, e:
        prev_down, prev_up, prev_wounded = (0, 0, 0)

    return (prev_down, prev_up, prev_wounded)

def write_result(i, result, filename):
    return_code = result if isinstance( result, ( int, long ) ) else result.return_code
    if return_code == 0:
        print("target_"+str(i), ":DEFACED", " Team: ", i)
        status = 'yellow' #Green
    elif return_code == 1:
        print("target_"+str(i), ":OK", " Team: ", i)
        status = 'green' #Green
    else:
        print("Return code", return_code, result)
        status = 'red'

    prev_down, prev_up, prev_wounded = read_result(filename, i)

    if status == 'green':
        prev_up += 1
    elif status == 'yellow':
        prev_wounded += 1
    elif status == 'red':
        prev_down += 1

    data = {i: {'status': status, 'up': str(prev_up), 'wounded': str(prev_wounded),
                                'down': str(prev_down)}}

    print("Host: target%d, Status: %s, up: %d, wounded: %d, down: %d" % (i, status, prev_up, prev_wounded, prev_down))

    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))


@roles('red')
@parallel
def do_xss():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        for i in range(1, 41):
            filename = "results/" + env.xsstaskanem + str(i) + ".json"
            try:
                result = run("/root/cyber0lympics/Scripts/Scoring/general/grepUrlString.sh 'http://gov{0}.ban/?page=search.html&q=<script>stuff=5</script>' '<script>stuff=5</script>'".format(i))
                write_result(i, result, filename)

            except Exception, e:
                print(e)
                write_result(i, 2, filename)

@roles('red')
@parallel
def do_path():
    status = 'gray'
    with settings(
            hide('warnings', 'running', 'stdout', 'stderr'),
            warn_only=True
    ):
        for i in range(1, 41):
            filename = "results/" + env.pathtaskanem + str(i) + ".json"
            try:
                result = run("/root/cyber0lympics/Scripts/Scoring/general/grepUrlString.sh 'http://gov{0}.ban/?page=../../shop.ban/Shop/index.php' 'The Shop Administration'".format(i))
                write_result(i, result, filename)

            except Exception, e:
                print(e)
                write_result(i, 2, filename)

def run_xss():
    while True:
        execute(do_xss)
        sleep(1)
        write_json(env.xsstaskanem)
        execute(update_scoring, task_prefix=env.xsstaskanem)
        print("SLEEP 10 xss (safe to stop)")
        sleep(10)

def run_path():
    while True:
        execute(do_path)
        sleep(1)
        write_json(env.pathtaskanem)
        execute(update_scoring, task_prefix=env.pathtaskanem)
        print("SLEEP 10 path traversal (safe to stop)")
        sleep(10)