#!/bin/bash
url=${1%/}
search_string=$2
user_agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0"

curlBody=$(curl --connect-timeout 3 -m 3 -A "$user_agent" $url)
curlResult=$?

grepBody=$(echo "$curlBody" | grep -i "$search_string")
grepResult=$?

if [ "$curlResult" -eq 0 -a "$grepResult" -eq 0 ]
then
    exit 0
elif [ "$curlResult" -ne 0 ]
then
    exit 2
elif [ "$curlResult" -eq 0 -a "$grepResult" -eq 1 ]
then
    exit 1
else
    exit 3
fi