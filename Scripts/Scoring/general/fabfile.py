# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep
import sys
import io, json


# fab -i ~/.ssh/id_rsa_scoring --skip-bad-hosts -P  --warn-only is_backup  --hide running

# Uus ühendus
# fab --warn-only is_backup

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = False
fabric.state.output.warn = False
env.user = 'root'

result, kali, hosting, fw = {}, {}, {}, {}

env.roledefs = {
    'scoring': ['scoreboard.ban', 'robot.itcollege.ee'],
    'red': ['192.168.100.101','192.168.100.102','192.168.100.103'],
    'green': ['192.168.100.101','192.168.100.102','192.168.100.103'],
    'kali': [], 'hosting': [], 'fw': [] }

for i in range(1, 41):
    hostname = "10.1.%s.66" % str(i + 10)
    env.roledefs['kali'].append("10.1.%s.66" % str(i + 10))
    kali[hostname] = i

for i in range(1, 41):
    hostname = "10.1.%s.200" % str(i + 10)
    env.roledefs['hosting'].append("10.1.%s.200" % str(i + 10))
    hosting[hostname] = i

for i in range(1, 41):
    hostname = "10.2.%s.2" % str(i + 10)
    env.roledefs['fw'].append("10.2.%s.2" % str(i + 10))
    fw[hostname] = i

print("before: %s" % fabric.state.output)


@roles('red')
@parallel
def network():
    run('dhclient eth0')

@roles('red', 'green')
@parallel
def prepare():
    try:
        run('test -d /root/cyber0lympics || cd /root && git clone git@bitbucket.org:magavdraakon/cyber0lympics.git')
        with cd('/root/cyber0lympics/'):
            run('git pull')
            run('apt-get install curl -y')
            run('apt-get install apache2-utils -y')

            try:
                run('test -x /root/cyber0lympics/Scripts/Scoring/ddos/dos.sh')
            except Exception, e:
                print("Ilge seakisia")

            try:
                run("/root/cyber0lympics/Scripts/Scoring/Watir/WatirSetup.sh")
            except Exception, e:
                print("===!==!==!== WATIR NOT SET UP PROPERLY ==!==!==!===")
                print("ERROR:", e)


    except fabric.exceptions.NetworkError, ne:
        print("Error connecting to ", env.host)
    except Exception, e:
        print("Connected to ", env.host)
        print("Jama majas", type(e), e)
