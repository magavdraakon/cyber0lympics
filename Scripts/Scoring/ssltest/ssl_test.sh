#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# License The MIT License (MIT) (included in project tree)
#
# $1 - a host to test against insecure ssl configuration

#exit 0 - site is up and not vulnerable
#exit 1 - site is up and vulnerable
#exit 2 - site is down
#exit 3 - wrong number of parameters

#$1 - a host like hosting1.ban

user_agent="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0"


if [ $# -eq 1 ]
then
    curl --insecure --connect-timeout 4 -m 4 -A "$user_agent" https://$1/ | grep -i "His Majesty Bananius XII Magnus himself"  || exit 2
    echo -n| openssl s_client -connect $1:443 -ssl3 && exit 1
    [ -x /root/heartbleed.py ] || wget https://gist.githubusercontent.com/eelsivart/10174134/raw/5c4306a11fadeba9d9f9385cdda689754ca4d362/heartbleed.py -O /root/heartbleed.py && chmod +x /root/heartbleed.py
    ./heartbleed.py $1 | grep 'server is vulnerable' && exit 1


    exit 0
else
    echo "Use $0 hostname"
    exit 3
fi
