# coding: utf-8
from __future__ import print_function
from __future__ import unicode_literals
# from fabric.api import run, env
from fabric.api import *
import fabric
from time import sleep

import io, json


# fab -i ~/.ssh/id_rsa_scoring --skip-bad-hosts -P  --warn-only is_backup  --hide running

# Uus ühendus
# fab --warn-only is_backup

env.hosts = []
env.skip_bad_hosts = True
env.key_filename = '~/.ssh/id_rsa_scoring'
env.skip_bad_hosts = True
fabric.state.output.running = True
fabric.state.output.warn = False
env.user = 'root'
env.taskFilename = 'taskDDOS'

result = dict()

env.roledefs = {'deface_gov': ['192.168.100.101'], 'deface_shop': ['192.168.100.101'], 'scoring': ['scoreboard.ban', 'robot.itcollege.ee'], 'red': ['192.168.100.101','192.168.100.102','192.168.100.103'], 'kali': [], 'hosting': [], 'fw': [] }

for i in range(1, 2):
    hostname = "10.1.%s.66" % str(i + 10)
    env.hosts.append("10.1.%s.66" % str(i + 10))
    result[hostname] = i

print("before: %s" % fabric.state.output)


@roles('red')
@parallel
def network():
    run('dhclient eth0')

def read_result(filename, i):
    try:
        with io.open(filename) as fh:
            line = fh.readline()
            if line:
                prev = json.loads(line, encoding='utf-8')
                prev_up = int(prev[str(i)]['up'])
                prev_down = int(prev[str(i)]['down'])
                # print('Host:', "target_"+str(i), 'previous: up', prev_up, 'previous: wounded', prev_wounded, 'previous: down', prev_down)
            else:
                prev_down, prev_up = (0, 0)
            fh.close()
    except IOError, e:
        prev_down, prev_up = (0, 0)

    return (prev_down, prev_up)

def write_result(i, result, filename):
    return_code = result if isinstance( result, ( int, long ) ) else result.return_code
    if return_code == 0:
        print("target_"+str(i), ":OK", " Team: ", i)
        status = 'green' #Green
    else:
        print("Return code", return_code, result)
        status = 'red'

    prev_down, prev_up = read_result(filename, i)

    if status == 'green':
        prev_up += 1
    elif status == 'red':
        prev_down += 1

    data = {i: {'status': status, 'up': str(prev_up),
                                'down': str(prev_down)}}

    print("Host: target%d, Status: %s, up: %d, down: %d" % (i, status, prev_up, prev_down))

    with io.open(filename, 'w', encoding='utf-8') as f:
        f.write(unicode(json.dumps(data, ensure_ascii=False)))

def write_json():
    print("Writing json file")
    with io.open(env.taskFilename+'.json','w',encoding='utf-8') as of:
        of.write('{')
        for i in range(1,41):
            fh = open('results/'+env.taskFilename+str(i)+'.json')
            line = fh.readline()
            if i < 40:
                of.write(line[1:-1]+',\n')
            else:
                of.write(line[1:-1]+'\n')
            fh.close()
        of.write('}')


@roles('scoring')
def update_scoring():
    print("updating scoring")
    with cd('/var/cyber0lympics/ui/json/'):
        put(env.taskFilename+'.json', env.taskFilename+'.json')


@roles('red')
@parallel
def do_ddos(duration):
    try:
        for i in range(1, 41):
            print("attacking blog%s.ban" % i)
            pingresult = run("ping -c2 blog%s.ban" % i)
            if pingresult.return_code == 0:
                result = run("/root/cyber0lympics/Scripts/Scoring/ddos/dos.sh http://blog%d.ban/ 500 hello %d" % (i, duration))

    except fabric.exceptions.NetworkError, ne:
        print("Error connecting to ", env.host)
    except Exception, e:
        print("Connected to ", env.host)
        print("Jama majas", type(e), e)

@roles('red')
@parallel
def check_sites_availability():
    status = 'gray'
    for i in range(1, 41):
        filename = "results/" + env.taskFilename + str(i) + ".json"
        try:
            curlGrepResult = run("/root/cyber0lympics/Scripts/Scoring/general/grepUrlString.sh http://blog%d.ban/ Banania" % i)
            write_result(i, curlGrepResult, filename)

        except Exception, e:
            print(e)
            write_result(i, 2, filename)

def run_ddos():
    while True:
        duration=121
        print("START DDOS")
        execute(do_ddos, duration=duration)

        # Update score && also acts as sleep between attacks
        for i in range(1, (duration/20)+1):
            print("START AVAILABILITY")
            execute(check_sites_availability)
            print("SLEEP 5 (do not cancel)")
            sleep(5)
            write_json()
            execute(update_scoring)
            print("SLEEP 10 (safe to stop, attack still going)")
            sleep(10)
        print("SLEEP 10 (safe to stop, about to start new ddos attack)")
        sleep(10)