#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com

# Scipt that creates static routes in PfSense (GAMENET-FIREWALL)
# * Custom XML file is in PfSense GUI -> Diagnostics -> Edit File -> Browse -> conf -> config.xml

# License The MIT License (MIT) (included in project tree)


LC_ALL=C

# Create custom routes 
for i in {10..50}
do


echo "		<route>
			<network>10.1.$i.0/24</network>
			<gateway>GW$i</gateway>
			<descr/>
		</route>
"

done
