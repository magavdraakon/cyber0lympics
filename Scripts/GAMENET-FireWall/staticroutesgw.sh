#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com

# Script that creates custom gateways in PfSense (GAMENET-FIREWALL)
# * Custom XML file is in PfSense GUI -> Diagnostics -> Edit File -> Browse -> conf -> config.xml

# License The MIT License (MIT) (included in project tree)

LC_ALL=C

# Create custom gateways
for i in {10..50}
do


echo "		<gateway_item>
			<interface>lan</interface>
			<gateway>10.80.65.$i</gateway>
			<name>GW$i</name>
			<weight/>
			<ipprotocol/>
			<interval/>
			<descr/>
		</gateway_item>"

done
