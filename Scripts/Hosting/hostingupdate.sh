#!/bin/bash
# Update script for Hosting
# 
# MIT License 

cd /root/deleteme/cyber0lympics/

git pull 

cd /root/deleteme/cyber0lympics/Targets/

# Remove old Gov website & copy new from bitbucket 

rm /var/www/gov.ban/Gov/ -r && cp Gov/ /var/www/gov.ban/ -a
	
	chown www-data:www-data /var/www/ -R

# Remove old SHop website & copy new from bitbucket

rm /var/www/shop.ban/Shop/ -r && cp Shop/ /var/www/shop.ban/ -a 

	chown www-data:www-data /var/www/ -R

# Restart mysql & apache

cd /root/deleteme/cyber0lympics/Targets/Shop

mysql -uroot -pstudent < setup.sql

service apache2 stop && service apache2 start


