#!/usr/local/bin/bash

#author Margus Ernits
#license MIT
#http://hosting.ban/cgi-bin/status.cgi

set -f

echo "Content-type: text/html; charset=iso-8859-1"
echo

#echo CGI/1.0 test script report:
#echo
echo "<!DOCTYPE html>"
echo "<html>"
echo "<head><title>Server status</title></head><body>"
echo "Server status - $(date)"

echo "<pre>
$(df -h|head -n2|tail -n1)

$(uprecords)


$(finger)

Apache processes running: $(ps -ef|grep httpd| grep grep -v|wc -l)

Varnish processes running: $(ps -ef|grep varnish|grep grep -v| wc -l)
</pre>"
#echo argc is $#. argv is "$*".
echo "</body>"
echo "</html>"
