# This is the dynamic editing of /etc/hosts file for Hosting machine

file { '/etc/hosts':
  ensure => present,
}->
file_line { 'localhost':
  path => '/etc/hosts',  
  line => "127.0.1.1	${hostnamealias}${dynamicip}.ban	${hostnamealias}${dynamicip}",
  match => "127.0.1.1.*",
}

# Dynamic editing of /etc/hostname

file { '/etc/hostname':
  ensure  => 'file',
  content => "${hostnamealias}${dynamicip}",
  group   => '0',
  mode    => '644',
  owner   => '0',
}

# This is the dynamic editing of /etc/network/interfaces for Kali, Hosting and Bandemia machines.
# Puppet module example42/network

$number = 10 + "${dynamicip}"

case $hostnamealias {
      kali: { $eth0 = "10.1.${number}.66" }
      hosting: { $eth0 = "10.1.${number}.200" } 
      bandemia: { $eth0 = "10.1.${number}.212" }
      default: { fail("Unrecognized host and cannot ") }
    }
   network::interface { 'eth0': 
      ipaddress => $eth0,
      netmask   => '255.255.255.0',
      gateway =>  "10.1.${number}.254",
      dns_nameservers => '10.80.65.5 8.8.8.8',
    }

# Add IP aliases

file_line { 'IP-alias 1':
  line => "
auto eth0:1
iface eth0:1 inet static
  address 10.1.${number}.201
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 2':
  line => "
auto eth0:2
iface eth0:2 inet static
  address 10.1.${number}.202
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 3':
  line => "
auto eth0:3
iface eth0:3 inet static
  address 10.1.${number}.203
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 4':
  line => "
auto eth0:4
iface eth0:4 inet static
  address 10.1.${number}.204
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 5':
  line => "
auto eth0:5
iface eth0:5 inet static
  address 10.1.${number}.205
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 6':
  line => "
auto eth0:6
iface eth0:6 inet static
  address 10.1.${number}.206
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 7':
  line => "
auto eth0:7
iface eth0:7 inet static
  address 10.1.${number}.207
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 8':
  line => "
auto eth0:8
iface eth0:8 inet static
  address 10.1.${number}.208
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 9':
  line => "
auto eth0:9
iface eth0:9 inet static
  address 10.1.${number}.209
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'IP-alias 10':
  line => "
auto eth0:10
iface eth0:10 inet static
  address 10.1.${number}.210
  netmask 255.255.255.0",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}