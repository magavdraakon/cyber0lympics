# This is the dynamic editing of /etc/hosts file for Kali machine

file { '/etc/hosts':
  ensure => present,
}->
file_line { 'localhost':
  path => '/etc/hosts',  
  line => "127.0.1.1	${hostnamealias}${dynamicip}.ban	${hostnamealias}${dynamicip}",
  match => "127.0.1.1.*",
}

# Dynamic editing of /etc/hostname

file { '/etc/hostname':
  ensure  => 'file',
  content => "${hostnamealias}${dynamicip}",
  group   => '0',
  mode    => '644',
  owner   => '0',
}

# This is the dynamic editing of /etc/network/interfaces for Kali, Hosting and Bandemia machines.
# Puppet module example42/network

$number = 10 + "${dynamicip}"

case $hostnamealias {
      kali: { $eth0 = "10.1.${number}.66" }
      hosting: { $eth0 = "10.1.${number}.200" } 
      bandemia: { $eth0 = "10.1.${number}.212" }
      default: { fail("Unrecognized host and cannot ") }
    }
   network::interface { 'eth0': 
      ipaddress => $eth0,
      netmask   => '255.255.255.0',
      gateway =>  "10.1.${number}.254",
      dns_nameservers => '10.80.65.5 8.8.8.8',
    }
