#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# Scipt Installs puppet from puppetlabs repos for Ubuntu
# License The MIT License (MIT) (included in project tree)
#


# exit 0 backup is done
# exit 1 half done
# exit 2 not done



[ -d /backup ] || exit 2

find /backup -size +10k > /dev/null && exit 0

grep -i "insert" /backup/* -R  && grep -i "Banania" /backup/* -R && exit 0 || exit 1
