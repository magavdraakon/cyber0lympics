#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com
# Date - 11.02.2015
# License MIT 

# Script that imports CyberOlympic ovas from https://i-tee.itcollege.ee:4433/ovas/ 


cd /var/labs/ovas || {

	echo "There is no correct directory. Exiting!"
	exit 
}


# Machines to import are:

# * CyberOlympics-Bandemia-template 
# * CyberOlympics-Hosting-template 
# * CyberOlympics-Kali-template 
# * CyberOlympics-Botnet-IRC-template 
# * CyberOlympics-Scoring-template 
# * CyberOlympics-GAMENET-DNS-template 
# * CyberOlympics-GAMENET-FireWall-template 
# * CyberOlympics-management-margus-template
# * CyberOlympics-Router-template
# * CyberOlympics-FW-template
# * CyberOlympics-Traffic-generator
# * CyberOlympics-management-johannes-template
# * CyberOlympics-Management-Katrin
# * CyberOlympics-Traffic-terrorist1
# * CyberOlympics-Traffic-terrorist2
# * CyberOlympics-Traffic-terrorist3
# * CyberOlympics-Traffic-terrorist4
# * CyberOlympics-Traffic-terrorist5
# * CyberOlympics-Traffic-terrorist6

# Import the ovas to CyberOlympian machines 
# * hostsToExport is array where the machine names should be added

hostsToImport=(CyberOlympics-Traffic-terrorist3 CyberOlympics-Traffic-terrorist4 CyberOlympics-Traffic-terrorist5 CyberOlympics-Traffic-terrorist6)

for ova in ${hostsToImport[@]}; do 

# If the ova exists already then delete it first

[ -f /var/labs/ovas/$ova.ova ] && rm -f /var/labs/ovas/$ova.ova
	
# Download ovas from i-tee

	echo "Will start downloading machine $ova ..."
		wget --user cyber --password "V5kwR=s4=@t#cY"  https://i-tee.itcollege.ee:4433/ovas/$ova.ova || {
			
			echo "Can't download $ova from https://i-tee.itcollege.ee:4433/ovas/ Exiting!"
			exit 1
		}

	sleep 2

# TODO - Won't work properly - some of the files are imported to /root/Virtualbox VMS. Can't set it with vboxmanage import -n templatename either

# Import ovas into the machine
# * If the virtual machine exists already then delete it first 

#	[ -d /var/labs/VirtualBox\ VMs/$ova/ ] && vboxmanage unregistervm $ova && mv /var/labs/VirtualBox\ VMs/$ova/ /var/labs/VirtualBox\ VMs/backups/$ova

#	echo "Starting importing of machine $ova ..."
#		vboxmanage import --vsys 0 --unit 12 --disk /var/labs/VirtualBox\ VMs/$ova/$ova-disk1.vmdk --vsys 0 --unit 13 --disk /var/labs/VirtualBox\ VMs/$ova/$ova-disk2.vmdk $ova.ova || {

#			echo "Can't import machine $ova Exiting!"
#			exit 1
#		}
	
#	cp /root/VirtualBox\ VMs/$ova/$ova.vbox /var/labs/VirtualBox\ VMs/$ova/$ova.vbox
#	sleep 2

# Initialize the machines 
# TODO - Must run as subshell 

#	echo "Initalizing the machine $ova ..."
#	vboxheadless --startvm $ova || {

#		echo "Can't start the machine $ova Exiting!"
#		exit 1
#	}

#	sleep 2

done