#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com
# Date - 11.02.2015
# License MIT 

# Script that exports CyberOlympic ovas from i-tee.itcollege.ee and then imports them to Cyberlab i-tee

# Connect to i-tee.itcollege.ee

ssh root@i-tee.itcollege.ee && ./i.sh || {

	echo "Can't connect to i-tee.itcollege.ee"
	exit 
}


# Check if the script is run in the correct machine and user

HOSTNAME=$(hostname -f)
USERNAME=$(whoami)

if [ $HOSTNAME = "i-tee.itcollege.ee" ] && [ $USERNAME = "vbox" ]; then
	
	echo "Correct machine!"

else 
	
	echo "Not in i-tee.itcollege.ee . Exiting!"
	exit 
fi

[ -d ~/ovas/ ] || {

	echo "There is no correct directory. Exiting!"
	exit 
}

cd ~/ovas/

# Machines to export are:

# * CyberOlympics-Bandemia-template 
# * CyberOlympics-Hosting-template 
# * CyberOlympics-Kali-template 
# * CyberOlympics-Botnet-IRC-template 
# * CyberOlympics-Scoring-template 
# * CyberOlympics-GAMENET-DNS-template 
# * CyberOlympics-GAMENET-FireWall-template 
# * CyberOlympics-management-margus-template
# * CyberOlympics-Router-template
# * CyberOlympics-FW-template
# * CyberOlympics-Traffic-generator
# * CyberOlympics-management-johannes-template
# * CyberOlympics-Management-Katrin
# * CyberOlympics-Traffic-terrorist11
# * CyberOlympics-Traffic-terrorist12
# * CyberOlympics-Traffic-terrorist13
# * CyberOlympics-Traffic-terrorist14
# * CyberOlympics-Traffic-terrorist15
# * CyberOlympics-Traffic-terrorist16

# Just add the machine names accordingly to the hostsToExport array 

# Export the machines to ova

hostsToExport=(CyberOlympics-Bandemia-template)

for ova in ${hostsToExport[@]}; do 

	echo "Will start exporting machine $ova"
	vboxmanage export $ova --output $ova.ova --ovf20

done

echo "All machines exported!"


