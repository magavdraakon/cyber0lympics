#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com
# Date - 17.01.2015

# Script for installing Zabbix agent 
# * Kali Linux needs Debian packages, so don't use this script for Debian!
# * Make sure that visudo has line "zabbix ALL=NOPASSWD: ALL"

# License The MIT License (MIT) (included in project tree)

# Check for root rights 

[ $UID -ne 0 ] && {

	echo "Not root! Run script with root rights! Exiting!"
	exit 2
}

# Add Zabbix application PPA

wget http://repo.zabbix.com/zabbix/2.4/ubuntu/pool/main/z/zabbix-release/zabbix-release_2.4-1+trusty_all.deb && dpkg -i zabbix-release_2.4-1+trusty_all.deb || {

	echo "Can't download Zabbix package or dpkg -i the package! Exiting!"
	exit 1
}

# Install the Zabbix agent

apt-get update && apt-get install zabbix-agent || {

	echo "Can't install Zabbix agent! Exiting!"
	exit 1

}

# Edit Zabbix agent config file
# * Add server IP's & agent hostname
# * Enable remote commands & allow Zabbix to run them as sudo

sed -i 's/Server=127.0.0.1/Server=172.16.0.10/' /etc/zabbix/zabbix_agentd.conf
sed -i 's/ServerActive=127.0.0.1/ServerActive=172.16.0.10/' /etc/zabbix/zabbix_agentd.conf
sed -i "s/Hostname=Zabbix server/Hostname=$HOSTNAME/" /etc/zabbix/zabbix_agentd.conf

sed -i "s/# EnableRemoteCommands=0/EnableRemoteCommands=1/" /etc/zabbix/zabbix_agentd.conf
echo "zabbix ALL=NOPASSWD: ALL" >> /etc/sudoers

# Restart Zabbix agent

service zabbix-agent restart && echo "Everything went well, Your Zabbix version is:" && zabbix_agent --version || {

	echo "Oops, looks like something went wrong!"
	exit 1

}