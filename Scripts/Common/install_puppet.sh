#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# Scipt Installs puppet from puppetlabs repos for Ubuntu
# License The MIT License (MIT) (included in project tree)
#
# exit 1  - Not in ubuntu
# exit 2  - can not download puppetlabs-release-*.deb


# Test -e Ubuntu:) #TODO #LOWPRIORITY

if [[ "$(lsb_release -is)" = "Ubuntu" ]]
then
    echo "Ubuntu!"
    else
    echo "Not Ubuntu!"
    exit 1
fi
# Set repos

cd /root

UV=$(lsb_release -cs)

wget http://apt.puppetlabs.com/puppetlabs-release-$UV.deb || {

    echo "Can not dowload puppetlabs-release-$UV.deb"
    exit 2
}

sudo dpkg -i puppetlabs-release-$UV.deb
rm puppetlabs-release-$UV.deb

sudo apt-get update -qq
sudo apt-get install puppet -y

# Install Puppet module for configuring /etc/network/interfaces

sudo puppet module install example42-network 




