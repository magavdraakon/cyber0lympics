ssh_authorized_key { 'margus.ernits@ban':
    user => 'root',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDCB6/nhkQk6wxitjclAbRw8VdyiwZ9c+w7QwU7scLhwT5+sJ6dGQMDhduyltVLS/PBR/RiFWyGVeYGVO1aWq2eWWtrqIdoqePkd6xCU2HZNolLMZdYu6rR7QDrfXEptvK1i15Zk3yqQcXaWvIwQjmA2onyhiX62DJDFaYX++dkoCUVFE3M257yUR3WTnX9+KTef1HHaW4nrIeMNTq5qNC67NKVs+PoWa3W3gCBX07q7RBJ3FiU4ISek5b02Ps56WxIwA0eX/W3olDpoOsaCDDuN3u/mpXXChFxF8sOFqiDuznVhIlXmN2vv69EUUz4DosdW5ONb3aYplW46NQ5m0gx',
}

ssh_authorized_key { 'johannes.tammekand@ban':
    user => 'root',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQChbFAyLLgr1EaD5y9oX077v59MDE+Ip857eyk5CkiVjIcGX6TyQ20BJcLaKcIFiTaB6umPx/KfJoVROXgEB4V8CT+pf1U4huDGqqCC7gY9zEsLJmdKca8t+djY5l/Bersg/3JWT3+ryJx9+kf7DYZTxWS7XWr6jd0hKHhdOZ2nrkxtpI+k6sfC9KRXvBmBLzWG7sSFQFwtJira9EshGE8aiI2i2Chya71ErUsyTqyCo38BWhLEzSM47dr+VySNoAIYtNRl6TEDf3h6D6ULM2S7P+jZ3rGTQrqvwiGPfgHqVnFlOcZFYTehx6//12YKdoy/Vvt0U5y6EUPgUhOc7Qop',
}


ssh_authorized_key { 'scoring@ban':
    user => 'root',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDElEVMBQL7zzWf+MjLHLnaOKNlhBeWelDw34xN6AbIJbWGOAskqk+YEypbOCtS8u7QIEyeusMYs9qWiL9Sggjflf9zUzEpRGZaMe+lqFT8vfOUj5DjIrNHWMJ2vlDke3gDhfZjJhCAcyzK1ASUsvbasXa6ymtWz9e1hUqNZrnH4okYBB8FIDE9BDCtT3w/09J/8idhguV2Ev19fT+fdlQSIalgl8cZj/fsJCnxX3yq1EUVftxmgETqzrNrzmjznsscxtyt9PIRs/EP8Ch6KlhJkiu86UX9hyFPsdfS+Iw4LRKz0R4TWlyDHVAgQvMMV3rTjL8HGA5eery+RoJdqn+z',
}

ssh_authorized_key { 'tiia@cyber-management':
    user => 'root',
    type => 'rsa',
    key => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDjjZF6znX0hC2q3Zycg4Q9TgnB/myGwouhwgi62L+Xn2EWvdDdQ5zFmbtuyUqaCNPdrAFC0Ppsdg6jVHOt7nYzG67S2BVbh9ZG3llvNw1cDidmb6zd1jcDTd8+t3FD0TzcWQ//n3bWcKphAhUCkqhIEE49J6atZt+3N6HTNCDhlT1DHe0eSCcMA7/oFVwgB4yYnE0JJcsMHdr/Z+jQIxaTO5FwkUQmUM5CVnQRJfC/vloCFkcXTw8PvR8wYIEj7S2EshIKWhB296mB9lesjRe0g1Q9NNT5dt/mIZrEBPtKiaNzS9iTsLG/V8egaZBFpG4Oh/bSX9EWvVK4tf3dPoif',
}

