#!/bin/bash
# Author Johannes Tammekänd johannes.tammekand@gmail.com
# Date - 01.12.2014
# Script for making Puppet facts for CyberOlympics VM-s
# License The MIT License (MIT) (included in project tree)

export LC_ALL=C

KALI="CyberOlympics-Kali"
HOSTING="CyberOlympics-Hosting"
FIREWALL="CyberOlympics-FW"
BANDEMIA="CyberOlympics-Bandemia"
ROUTER="CyberOlympics-Router"

# Create the directory if it is not present 

[ -d /etc/facter/facts.d/ ] || mkdir -p /etc/facter/facts.d/

# Taking information from BIOS

DMIDECODEVERSION="$(dmidecode -s bios-version | cut -d'-' -f1,2)"
DMIDECODERELEASEDATE="$(dmidecode -s bios-release-date)"
DYNAMICIPNUMBER=$(printf "%s\n" "${DMIDECODERELEASEDATE//[![:digit:]]/}")
REGEXIP='^[0-9]+$' # Regex a number (integer)

# If the host machine is Kali Linux then make corresponding Puppet facters

if [ "$DMIDECODEVERSION" == "$KALI" ];
  then 

# If no number included will set everything to default 255

if [ "$DYNAMICIPNUMBER" == "" ];
  then

  echo "localhostname=kali255.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=kali" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=255" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

# If number provided then set facters with the number 

elif ! [[ $DYNAMICIPNUMBER == $REGEXIP ]]; 
  then

  echo "localhostname=kali$DYNAMICIPNUMBER.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=kali" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=$DYNAMICIPNUMBER" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

fi
fi

# If the host machine is Hosting then make corresponding Puppet facters

if [ "$DMIDECODEVERSION" == "$HOSTING" ];
  then 

# If no number included will set everything to default 255

if [ "$DYNAMICIPNUMBER" == "" ];
  then

  echo "localhostname=hosting255.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=hosting" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=255" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

# If number provided then set facters with the number 

elif ! [[ $DYNAMICIPNUMBER == $REGEXIP ]]; 
  then

  echo "localhostname=hosting$DYNAMICIPNUMBER.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=hosting" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=$DYNAMICIPNUMBER" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

fi
fi


# If the host machine is Firewall then make corresponding Puppet facters

if [ "$DMIDECODEVERSION" == "$FIREWALL" ];
  then 

# If no number included will set everything to default 255

if [ "$DYNAMICIPNUMBER" == "" ];
  then

  echo "localhostname=fw255.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=fw" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=255" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

# If number provided then set facters with the number 

elif ! [[ $DYNAMICIPNUMBER == $REGEXIP ]]; 
  then

  echo "localhostname=fw$DYNAMICIPNUMBER.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=fw" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=$DYNAMICIPNUMBER" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

fi
fi

# If the host machine is Bandeemia then make corresponding Puppet facters

if [ "$DMIDECODEVERSION" == "$BANDEMIA" ];
  then 

# If no number included will set everything to default 255

if [ "$DYNAMICIPNUMBER" == "" ];
  then

  echo "localhostname=bandemia255.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=bandemia" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=255" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

# If number provided then set facters with the number 

elif ! [[ $DYNAMICIPNUMBER == $REGEXIP ]]; 
  then

  echo "localhostname=bandemia$DYNAMICIPNUMBER.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=bandemia" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=$DYNAMICIPNUMBER" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

fi
fi

# If the host machine is Router then make corresponding Puppet facters

if [ "$DMIDECODEVERSION" == "$ROUTER" ];
  then 

# If no number included will set everything to default 255

if [ "$DYNAMICIPNUMBER" == "" ];
  then

  echo "localhostname=router255.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=router" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=255" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

# If number provided then set facters with the number 

elif ! [[ $DYNAMICIPNUMBER == $REGEXIP ]]; 
  then

  echo "localhostname=router$DYNAMICIPNUMBER.ban" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostnamealias=router" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "hostname=$DMIDECODEVERSION" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "dynamicip=$DYNAMICIPNUMBER" >> /etc/facter/facts.d/CyberOlympics.txt
  echo "username=$DMIDECODERELEASEDATE" >> /etc/facter/facts.d/CyberOlympics.txt

fi
fi