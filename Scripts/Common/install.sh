#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# Bootstrap script for CyberOlympics VM-s
# * Installs puppet agent
# * Installs root ssh keys for Margus and Johannes with puppet
# *
# License The MIT License (MIT) (included in project tree)
# exit 1 - can't install puppet
# exit 2 - not root

export SERVER=10.80.65.211
export LC_ALL=C

[ $UID -ne 0 ] && {

echo "You need root privileges to execute $0. Exiting!"
exit 2

}

apt-get update -qq
apt-get install wget curl -y

# download scripts
cd /root


FILE_LIST="install_puppet.sh rootkeys.pp PuppetFacts.sh rc.local networking.pp networkingfw.pp networkingrouter.pp"

[ -d /root/.config/.bootstrap/ ] || mkdir -p /root/.config/.bootstrap/
cd /root/.config/.bootstrap/

for f in $FILE_LIST
do
wget http://$SERVER/Scripts/Common/$f -O $f
chmod +x $f
done


# Edit /etc/rc.local file

sed -E 's@^exit 0@#CUTME\n[ -x /root/.config/.bootstrap/rc.local ] \&\& /root/.config/.bootstrap/rc.local\n\nexit 0\n\n@' -i /etc/rc.local

#Insall Puppet
[[ -x install_puppet.sh ]] && ./install_puppet.sh || {
echo "Can not install pupet! Exiting!"
exit 1
}

#root keys
[[ -r rootkeys.pp ]] && puppet apply rootkeys.pp || {
echo "Can't configure root public keys"
exit 3
}



