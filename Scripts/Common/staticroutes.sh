#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# Scipt  sets static routes for cyberolympic network
# use it in /etc/network/interfaces
# up /paht/to/this_script.sh
# License The MIT License (MIT) (included in project tree)
#
# exit 1  - Not in ubuntu
# exit 2  - can not download puppetlabs-release-*.deb

LC_ALL=C

#up route add -net 10.1.50.0 netmask 255.255.255.0 gw 10.80.65.50 dev eth1
for i in {1..40}
do

route add -net 10.1.$[10+$i].0 netmask 255.255.255.0 gw 10.80.65.$[10+$i] dev eth1
done
