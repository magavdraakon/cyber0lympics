#!/bin/bash
# Authors Johannes Tammekänd johannes.tammekand@gmail.com & Margus Ernits margus.ernits@gmail.com
# Bootstrap script for configuring VM-s
# License The MIT License (MIT) (included in project tree)

KALI="CyberOlympics-Kali"
HOSTING="CyberOlympics-Hosting"
FIREWALL="CyberOlympics-FW"
BANDEMIA="CyberOlympics-Bandemia"
ROUTER="CyberOlympics-Router"
TEMPLATE="VirtualBox"

# Get parameters from dmidecode 

DMIDECODEVERSION="$(dmidecode -s bios-version | cut -d'-' -f1,2)"
DMIDECODERELEASEDATE="$(dmidecode -s bios-release-date)"
DYNAMICIPNUMBER=$(printf "%s\n" "${DMIDECODERELEASEDATE//[![:digit:]]/}")
IPNUMBER=$(($DYNAMICIPNUMBER + 10))

# Check if the machine is template, if yes then exit

if [ "$DMIDECODEVERSION" == "$TEMPLATE" ]; then 

	echo "This is template machine! Exiting!"
	exit

fi

# Add virtualhosts for Hosting and Bandemia

if [ "$DMIDECODEVERSION" == "$BANDEMIA" ]; then 

echo "<VirtualHost 10.1.$IPNUMBER.212:80>
	
	ServerName bandemia$DYNAMICIPNUMBER.ban
	ServerAdmin webmaster@bandemia.ban
	DocumentRoot /var/www/drupal/

	<Directory /var/www/drupal>
        	AllowOverride All
    	</Directory>

	ErrorLog /var/log/apache2/drupal/error.log
	CustomLog /var/log/apache2/drupal/access.log combined

</VirtualHost>
<VirtualHost 10.1.$IPNUMBER.212:443>
  ServerName bandemia$DYNAMICIPNUMBER.ban
  DocumentRoot /var/www/drupal/
  SSLEngine on
  SSLCertificateFile      "/etc/ssl/certs/ban.pem"
  SSLCertificateKeyFile   "/etc/ssl/private/ban.key"
  SSLCACertificatePath    "/etc/ssl/certs"
  SSLProtocol             All
  SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA

</VirtualHost>" > /usr/local/apache2/conf/extra/bandemiaX.ban.conf

echo "<VirtualHost 10.1.$IPNUMBER.213:80>

        ServerName banania-admin$DYNAMICIPNUMBER.ban
        ServerAdmin webmaster@banania-admin.ban
        DocumentRoot /var/www/banania-admin/admin/

        <Directory /var/www/banania-admin>
                AllowOverride All
                Options -Indexes
        </Directory>

        ErrorLog /var/log/apache2/banania-admin/error.log
        CustomLog /var/log/apache2/banania-admin/access.log combined

</VirtualHost>
<VirtualHost 10.1.$IPNUMBER.213:443>
  ServerName banania-admin$DYNAMICIPNUMBER.ban
  DocumentRoot /var/www/banania-admin/admin/
  SSLEngine on
  SSLCertificateFile      "/etc/ssl/certs/ban.pem"
  SSLCertificateKeyFile   "/etc/ssl/private/ban.key"
  SSLCACertificatePath    "/etc/ssl/certs"
  SSLProtocol             All
  SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA

</VirtualHost>" > /usr/local/apache2/conf/extra/banania-adminX.ban.conf && service apache2 restart

elif [ "$DMIDECODEVERSION" == "$HOSTING" ];
  then 

chown www-data:www-data /var/wwww -R

sed -i "s@define('WP_HOME','http://blog.ban');@define('WP_HOME','http://blog$DYNAMICIPNUMBER.ban');@" /var/www/blog.ban/wordpress/wp-config.php
sed -i "s@define('WP_SITEURL','http://blog.ban');@define('WP_SITEURL','http://blog$DYNAMICIPNUMBER.ban');@" /var/www/blog.ban/wordpress/wp-config.php
#define('WP_HOME','http://blog.ban');
#define('WP_SITEURL','http://blog.ban');

echo "<VirtualHost *:80>

        ServerName gov$DYNAMICIPNUMBER.ban
        DocumentRoot /var/www/gov.ban/Gov/
        DirectoryIndex index.php
        #ErrorLog ${APACHE_LOG_DIR}/error.log
        #CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
<VirtualHost 10.1.$IPNUMBER.202:443>
  ServerName gov$DYNAMICIPNUMBER.ban
  DirectoryIndex index.php
  DocumentRoot /var/www/gov.ban/Gov/
  <Directory "/var/www/gov.ban/Gov/">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
  </Directory>
  SSLEngine on
  SSLCertificateFile      "/etc/ssl/certs/ban.pem"
  SSLCertificateKeyFile   "/etc/ssl/private/ban.key"
  SSLCACertificatePath    "/etc/ssl/certs"
  SSLProtocol             All
  SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA

</VirtualHost>"> /usr/local/apache2/conf/extra/govX.ban.conf

echo "<VirtualHost *:80>

        ServerName blog$DYNAMICIPNUMBER.ban
        DocumentRoot /var/www/blog.ban/wordpress
        DirectoryIndex index.php
        <Directory "/var/www/blog.ban/wordpress/">
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Require all granted
        </Directory>
        #ErrorLog ${APACHE_LOG_DIR}/error.log
        #CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>"> /usr/local/apache2/conf/extra/blogX.ban.conf


echo "<VirtualHost *:80>

        ServerName shop$DYNAMICIPNUMBER.ban
        DocumentRoot /var/www/shop.ban/Shop/
        DirectoryIndex index.php
        #ErrorLog ${APACHE_LOG_DIR}/error.log
        #CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

<VirtualHost 10.1.$IPNUMBER.203:443>
  ServerName shop$DYNAMICIPNUMBER.ban
  DocumentRoot /var/www/shop.ban/Shop/
  DirectoryIndex index.php
  <Directory "/var/www/shop.ban/Shop/">
    Options Indexes FollowSymLinks MultiViews
    AllowOverride None
    Require all granted
  </Directory>
  SSLEngine on
  SSLCertificateFile      "/etc/ssl/certs/ban.pem"
  SSLCertificateKeyFile   "/etc/ssl/private/ban.key"
  SSLCACertificatePath    "/etc/ssl/certs"
  SSLProtocol             All
  SSLCipherSuite          ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-DES-CBC3-SHA:ECDHE-ECDSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:DES-CBC3-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA

</VirtualHost>

" > /usr/local/apache2/conf/extra/shopX.ban.conf

service apache2 restart



fi

# Create Puppet facts
# *Check if file exists and is executable and if yes then execute it

[ -x /root/.config/.bootstrap/PuppetFacts.sh ] &&  /root/.config/.bootstrap/PuppetFacts.sh

# Check which networking manifest is needed to be applied and applied afterwards

if [ "$DMIDECODEVERSION" == "$ROUTER" ];
  then 

  puppet apply /root/.config/.bootstrap/networkingrouter.pp

elif [ "$DMIDECODEVERSION" == "$FIREWALL" ];
  then 

puppet apply /root/.config/.bootstrap/networkingfw.pp

elif [ "$DMIDECODEVERSION" == "$HOSTING" ];
  then 

puppet apply /root/.config/.bootstrap/networkinghosting.pp

service apache2 restart

elif [ "$DMIDECODEVERSION" == "$BANDEMIA" ];
  then 

puppet apply /root/.config/.bootstrap/networkingbandemia.pp

service apache2 restart

elif [ "$DMIDECODEVERSION" == "$KALI" ];
  then

puppet apply /root/.config/.bootstrap/networkingkali.pp

fi

# Add different DVBA users to the database 
# TODO - kasutajaid juurde tekitada

if [ "$DMIDECODEVERSION" == "$BANDEMIA" ]; then 

# Write SHA-1 hash to a static evidence file 

echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/AnanasiaHack/Hackzor

# Get a random number between 1-10

Randomnr="$(echo $RANDOM % 10 + 1 | bc)"

if [ "$Randomnr" == 1 ]; then 

	# Change student password in server 

	echo "student:horsetail" | chpasswd 

	# Delete & add users into table 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('horsetail'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Banania','Man','banania',MD5('moonshine'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Hack','Lulz','1337',MD5('darkhorse'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Pablo','Picasso','pablo',MD5('pablopicasso'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Banana','Squad','haxu',MD5('bananatrees'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	# Write participant name to grading file 

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	# Write participant password to grading file

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	# Write participant SHA-1 hash to grading file

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	# Write DBVA database users table to grading file 

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	# Write databse users passwords 

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:horsetail, banania:moonshine, 1337:darkhorse, pablo:pablopicasso, haxu:bananatrees" >> /root/.config/DB.txt

	# Copy the 
 

elif [ "$Randomnr" == 2 ]; then 

	echo "student:youcannot" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('youcannot'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Gazzo','Merda','gazzo',MD5('arebad'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Elmar','Tura','elmar',MD5('threeyellows'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Britta','Spears','britta',MD5('icantdance'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Dorian','Dork-Diesel','dorian',MD5('realweird'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:youcannot, gazzo:finnsarebad, elmar:threeyellows, britta:icantdance, dorian:realweirddude" >> /root/.config/DB.txt

elif [ "$Randomnr" == 3 ]; then 

	echo "student:lolpwned" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('lolpwned'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Auntie','Tillie','auntie',MD5('dumbuser'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Uncle','Sam','uncle',MD5('nastyhat'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','John','Silver','john',MD5('ninjapirate'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Snake','Plissken','snake',MD5('basketball'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:lolpwned, auntie:dumbuser, uncle:nastytophat, john:ninjapirate, snake:basketball" >> /root/.config/DB.txt

elif [ "$Randomnr" == 4 ]; then 

	echo "student:impissed" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('impissed'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Gaylord','Focker','gaylord',MD5('flying'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Pudimir','Vlatin','pudimir',MD5('baddestbadass'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Bruno','Banani','bruno',MD5('stinker'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Vello','Vint','vello',MD5('vodka'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:impissed, gaylord:flyingfocker, pudimir:baddestbadass, bruno:stinkerbanana, vello:vodkarules" >> /root/.config/DB.txt

elif [ "$Randomnr" == 5 ]; then 

	echo "student:noattempt" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('noattempt'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Dopey','Dwarf','dopey',MD5('nuisance'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Eugene','Belford','plague',MD5('evil'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Salmo','Nella','salmo',MD5('badstuff'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Mick','Dundee','croc',MD5('isaknife'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:noattempt, dopey:littlenuisance, plague:eeeeevviiiilll, salmo:badstuffflying, croc:thisisaknife" >> /root/.config/DB.txt

elif [ "$Randomnr" == 6 ]; then 

	echo "student:irule" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('irule'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Nonnie','Brainer','nobrainer',MD5('smartandnasty'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Juubal','Juhe','juubal',MD5('longwires'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Bang','Bugger','bang',MD5('stinker'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Beano','Baggot','beano',MD5('lotsandlots'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:irule, nobrainer:smartandnasty, juubal:dudehaslongwires, bang:realitystinks, beano:lotsandlotsofwind" >> /root/.config/DB.txt

elif [ "$Randomnr" == 7 ]; then 

	echo "student:bewarei" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('bewarei'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Ben','Notayahoo','ben',MD5('serious'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Dade','Murphy','crash',MD5('angelinacool'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Sly','Stallion','sly',MD5('bigbiceps'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Ham','Tyler','ham',MD5('gottogo'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:bewarei, ben:totallyserious, crash:angelinaiscool, sly:bigbiceps, ham:gotanoozee" >> /root/.config/DB.txt

elif [ "$Randomnr" == 8 ]; then 

	echo "student:leetleet" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('leetleet'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Paul','Atreides','paul',MD5('iambig'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Kate','Libby','acidburn',MD5('fireroof'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Lone','Starr','starr',MD5('ihatepizza'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Beavis','Butthead','bebu',MD5('thissucks'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:leetleet, paul:iamthebigone, acidburn:poolontheroof, starr:ihatepizza, bebu:manthissucks" >> /root/.config/DB.txt

elif [ "$Randomnr" == 9 ]; then 

	echo "student:getlost" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('getlost'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Betty','Salander','wasp',MD5('messwithme'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Emmanuel','Goldstein','cereal',MD5('tailguy'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Arnie','Black','arnie',MD5('fromaustria'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Peter','Porridge','pepor',MD5('bananasmoothie'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:getlost, wasp:dontmesswithme, cereal:thepigtailguy, arnie:comesfromaustria, pepor:bananasmoothie" >> /root/.config/DB.txt

elif [ "$Randomnr" == 10 ]; then 

	echo "student:beautifulsun" | chpasswd 

	echo "delete from users;" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('1','admin','admin','admin',MD5('beautifulsun'),'{$baseUrl}admin.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('2','Pomelo','Jomelo','pomjelo',MD5('fruits'),'{$baseUrl}banana.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('3','Captain','Jamerica','capmerica',MD5('goodguy'),'{$baseUrl}1337.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('4','Paint','Gyver','pagyver',MD5('bestshow'),'{$baseUrl}pablo.jpg');" | mysql -u root -psunshine bananiaadmin
	echo "INSERT INTO users VALUES ('5','Bad','Grampa','bgrampa',MD5('oldtimes'),'{$baseUrl}lizard.jpg');" | mysql -u root -psunshine bananiaadmin

	dmidecode -s bios-version > /root/.config/DB.txt 
	echo -e "\n" >> /root/.config/DB.txt

	echo "Password: horsetail" >> root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "SHA-1 hash from dmidecode:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo $(dmidecode -s bios-version) sKvC9zPjRqlnOzhUmL | sha1sum >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt

	echo "DBVA DB users table:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "SELECT * FROM users;" | mysql -u root -psunshine bananiaadmin >> /root/.config/DB.txt

	echo "DBVA DB users usernames-passwords:" >> /root/.config/DB.txt
	echo -e "\n" >> /root/.config/DB.txt
	echo "admin:beautifulsun, pomjelo:fruits, capmerica:goodguy, pagyver:bestshow, bgrampa:oldtimes" >> /root/.config/DB.txt


fi

# SCP hash files to Scoring machine 

scp /root/AnanasiaHack/Hackzor student@10.80.65.7:/home/student/bandemiahashes/ 

scp /root/.config/DB.txt student@10.80.65.7:/home/student/bandemiahashes/

fi


# Start the hostname service 
# * service hostname.sh is for Kali Linux 

service hostname.sh || service hostname start


# Restart adapters

ifdown --exclude=lo -a && ifup --exclude=lo -a

# Edit the Zabbix conf file hostname 

Hname="$(cat /etc/hostname)"
sed -i "s/Hostname=Zabbix server/Hostname=$Hname/" /etc/zabbix/zabbix_agentd.conf && service zabbix-agent restart

# Remove the bootstrap directory & cleanup /etc/rc.local & remove custom facts

	[ -d /root/.config/.bootstrap/ ] && rm -rf /root/.config/.bootstrap/
	[ -f /etc/facter/facts.d/CyberOlympics.txt ] && rm /etc/facter/facts.d/CyberOlympics.txt
	sed -i '/#CUTME/, +2d' /etc/rc.local

	[ -d /root/deleteme/ ] && rm -rf /root/deleteme/

exit 0
