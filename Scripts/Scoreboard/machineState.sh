#!/bin/bash

# Author - Johannes Tammekänd
# Date - 01.02.2015

# Script that automatically converts Zabbix API calls to proper JSON files for socreboard.ban
# 

# License The MIT License (MIT) 

# File to read

FILE=$1

# Clean the .json files 

echo "" > taskKali.json
echo "" > taskHosting.json
echo "" > taskBandemia.json
echo "" > taskFirewall.json
echo "" > taskRouter.json

# Create the start of the .json files

echo '{' > taskKali.json
echo '{' > taskHosting.json
echo '{' > taskBandemia.json
echo '{' > taskFirewall.json
echo '{' > taskRouter.json

# Read the file lines

while read line; do

# Regex the first number of the line

REGEX="$(echo $line | grep -o -E '[0-9]+' | head -1)"

# Check if Kali machine is up
# * Check if read line matches with status up code
# * If yes then in scoreboard will show green
# * If not then in scoreboard will show red 

if [ "$line" = "Host: Kali Workstation Participant "$REGEX" Host Status: 1" ]; then 


	echo '	"'$REGEX'": {"status": "done"},' >> taskKali.json

elif [ "$line" = "Host: Kali Workstation Participant "$REGEX" Host Status: 2" ]; then 

	echo '	"'$REGEX'": {"status": "danger"},' >> taskKali.json

fi

# Check if Hosting machine is up

if [ "$line" = "Host: Hosting - Participant "$REGEX" Host Status: 1" ]; then


        echo '  "'$REGEX'": {"status": "done"},' >> taskHosting.json

elif [ "$line" = "Host: Hosting - Participant "$REGEX" Host Status: 2" ]; then

        echo '  "'$REGEX'": {"status": "danger"},' >> taskHosting.json

fi

# Check if Bandemia machine is up

if [ "$line" = "Host: Bandemia - Participant "$REGEX" Host Status: 1" ]; then


        echo '	"'$REGEX'": {"status": "done"},' >> taskBandemia.json

elif [ "$line" = "Host: Bandemia - Participant "$REGEX" Host Status: 2" ]; then

        echo '	"'$REGEX'": {"status": "danger"},' >> taskBandemia.json

fi

# Check if Firewall machine is up

if [ "$line" = "Host: The Great Firewall of Banania - Participant "$REGEX" Host Status: 1" ]; then


        echo '	"'$REGEX'": {"status": "done"},' >> taskFirewall.json

elif [ "$line" = "Host: The Great Firewall of Banania - Participant "$REGEX" Host Status: 2" ]; then

        echo '	"'$REGEX'": {"status": "danger"},' >> taskFirewall.json

fi

# Check if Router machine is up

if [ "$line" = "Host: Router - Participant "$REGEX" Host Status: 1" ]; then


        echo '	"'$REGEX'": {"status": "done"},' >> taskRouter.json

elif [ "$line" = "Host: Router - Participant "$REGEX" Host Status: 2" ]; then

        echo '	"'$REGEX'": {"status": "danger"},' >> taskRouter.json

fi


done < $FILE

# Remove commas from the .json files

sed -i '$ s/,$//' taskKali.json
sed -i '$ s/,$//' taskHosting.json
sed -i '$ s/,$//' taskBandemia.json
sed -i '$ s/,$//' taskFirewall.json
sed -i '$ s/,$//' taskRouter.json


# Create the ending of the .json files 

echo '}' >> taskKali.json
echo '}' >> taskHosting.json
echo '}' >> taskBandemia.json
echo '}' >> taskFirewall.json
echo '}' >> taskRouter.json
