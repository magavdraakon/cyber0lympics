#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com
# Date - 17.01.2015

# Scipt that creates import_hosts.xml files for Zabbix machine for CyberOlympics
# * Import the .xml files in zabbix/hosts.php -> Import
# * Change the group name, host, hostname, ip accordingly 

# License The MIT License (MIT) (included in project tree)


LC_ALL=C

# Create custom routes 
for i in {10..30}; do

echo "<?xml version='1.0' encoding='UTF-8'?>
<zabbix_export>
    <version>2.0</version>
    <date>2015-01-17T12:43:51Z</date>
    <groups>
        <group>
            <name>CyberOlympics-Kali</name>
        </group>
    </groups>
    <hosts>
        <host>
            <host>kali$i</host>
            <name>Kali Workstation Participant $i</name>
            <proxy/>
            <status>0</status>
            <ipmi_authtype>-1</ipmi_authtype>
            <ipmi_privilege>2</ipmi_privilege>
            <ipmi_username/>
            <ipmi_password/>
            <templates>
                <template>
                    <name>Template OS Linux</name>
                </template>
            </templates>
            <groups>
                <group>
                    <name>CyberOlympics-LAN</name>
                </group>
            </groups>
            <interfaces>
                <interface>
                    <default>1</default>
                    <type>1</type>
                    <useip>1</useip>
                    <ip>10.1.$((10+$i)).66</ip>
                    <dns/>
                    <port>10050</port>
                    <interface_ref>if1</interface_ref>
                </interface>
            </interfaces>
            <applications/>
            <items/>
            <discovery_rules/>
            <macros/>
            <inventory/>
        </host>
    </hosts>
</zabbix_export>"  > import_hosts$i.xml 

done
