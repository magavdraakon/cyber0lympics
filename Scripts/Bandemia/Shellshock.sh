#!/bin/bash
# Author - Johannes Tammekänd - johannes.tammekand@gmail.com
# Date - 15.01.2015
# Script for installing Bash with Shellshock
# * 
# License The MIT License (MIT) (included in project tree)

export LC_ALL=C 

# Check for root rights

[ $UID -ne 0 ] && {

	echo "Not root! Run as root!"
	exit 2
}

CHECK = $(env x='() { :;}; echo OOPS' bash -c :)

# Check if the Bash is vulnerable to Shellshock

if [ $CHECK = "OOPS" ]; 
then

	echo "This machine is already vulnerable to Shellshock!"
	exit 

else 

	echo "This machine isn't vulnerable to Shellshock! Will downgrade Bash to 4.2"

	cd ~
	mkdir bash
	cd bash

# Download vulnerable Bash source from gnu.org

wget "https://ftp.gnu.org/gnu/bash/bash-4.2.tar.gz" && tar -xvzf bash-4.2.tar.gz || {

	echo "Can't download Bash! Exiting!" 
	exit 1
}
 
# Compile the code 

	cd bash-4.2
	./configure && make || {

		echo "Can't compile Bash! Possibly no C-compiler found!"
		exit 1
	}
	

# Install the Bash & Set it as the default shell

	make install 
	chsh -s /usr/local/bin/bash

# Cleanup

	rm -rf ~/bash

fi

echo "Open a new terminal and run env x='() { :;}; echo OOPS' bash -c :"
echo "If the output is OOPS then the new bash is vulnerable to Shellshock!"

# TODO - needs a new subshell to test. 

#if [ $CHECK = "OOPS" ]; 
#then

#	echo "Looks like everything went well, Bash is now vulnerable to Shellshock!"
#	exit 

#else 

#	echo "Oops, looks like something went wrong, Bash still isn't vulnerable to Shellshock!"

#fi 