#!/usr/bin/perl

# Original author - Alexei Vladishev
# Modified - Johannes Tammekänd
# Date - 01.02.2015

# Script for getting parameters with Zabbix API calls
# * Get's authentication token
# * Get parameters from API with given token
# * Print the parameters 

# License The MIT License (MIT) (included in project tree)

use 5.010;
use strict;
use warnings;
use JSON::RPC::Client;
use Data::Dumper;

# Authenticate yourself
my $client = new JSON::RPC::Client;
my $url = 'http://172.16.0.10/zabbix/api_jsonrpc.php';
my $authID;
my $response;

my $json = {
jsonrpc => "2.0",
method => "user.login",
params => {
user => "admin",
password => "kj2f8jwnKGnsk5*"
},
id => 1
};

$response = $client->call($url, $json);

# Check if response was successful
die "Authentication failed\n" unless $response->content->{'result'};

$authID = $response->content->{'result'};
print "Authentication successful. Auth ID: " . $authID . "\n";

# Get list of all hosts using authID

$json = {
jsonrpc=> '2.0',
method => 'host.get',
params =>
{
output => ['available','name'],# get only host id and host name
sortfield => 'name',        # sort by host name
},
id => 2,
auth => "$authID",
};
$response = $client->call($url, $json);

# Check if response was successful
die "host.get failed\n" unless $response->content->{result};

print "List of hosts\n-----------------------------\n";
foreach my $host (@{$response->content->{result}}) {
print "Host Up/Down: ".$host->{available}." Host: ".$host->{name}."\n";
}
