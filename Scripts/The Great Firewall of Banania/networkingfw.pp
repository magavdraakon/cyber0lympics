# This is the dynamic editing of /etc/hosts file 

file { '/etc/hosts':
  ensure => present,
}->
file_line { 'localhost':
  path => '/etc/hosts',  
  line => "127.0.1.1  ${hostnamealias}${dynamicip}.ban  ${hostnamealias}${dynamicip}",
  match => "127.0.1.1.*",
}

# Dynamic editing of /etc/hostname

file { '/etc/hostname':
  ensure  => 'file',
  content => "${hostnamealias}${dynamicip}",
  group   => '0',
  mode    => '644',
  owner   => '0',
}

# This is the dynamic editing of /etc/network/interfaces for Firewall machine.
# Puppet module example42/network

$number = 10 + "${dynamicip}"

case $hostnamealias {
      fw: { $eth0 = "10.2.${number}.2" }
      default: { fail("Unrecognized host and cannot ") }
    }
   network::interface { 'eth0': 
      ipaddress => $eth0,
      netmask   => '255.255.255.0',
      gateway =>  "10.2.${number}.1",
      dns_nameservers => '10.80.65.5 8.8.8.8',
    }

case $hostnamealias {
      fw: { $eth1 = "10.1.${number}.254" }
      default: { fail("Unrecognized host and cannot ") }
    }
   network::interface { 'eth1': 
      ipaddress => $eth1,
      netmask   => '255.255.255.0',
    }

