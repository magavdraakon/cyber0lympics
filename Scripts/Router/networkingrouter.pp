# This is the dynamic editing of /etc/hosts file 

file { '/etc/hosts':
  ensure => present,
}->
file_line { 'localhost':
  path => '/etc/hosts',  
  line => "127.0.1.1  ${hostnamealias}${dynamicip}.ban  ${hostnamealias}${dynamicip}",
  match => "127.0.1.1.*",
}

# Dynamic editing of /etc/hostname

file { '/etc/hostname':
  ensure  => 'file',
  content => "${hostnamealias}${dynamicip}",
  group   => '0',
  mode    => '644',
  owner   => '0',
}

# This is the dynamic editing of /etc/network/interfaces for Router machine.
# Puppet module example42/network

$number = 10 + "${dynamicip}"

case $hostnamealias {
      router: { $eth0 = "10.80.65.${number}"}
      default: { fail("Unrecognized host and cannot ") }
    }
   network::interface { 'eth0': 
      ipaddress => $eth0,
      netmask   => '255.255.255.0',
      gateway =>  "10.80.65.6",
      dns_nameservers => '10.80.65.5 8.8.8.8',
    }

case $hostnamealias {
      router: { $eth1 = "10.2.${number}.1" }
      default: { fail("Unrecognized host and cannot ") }
    }
   network::interface { 'eth1': 
      ipaddress => $eth1,
      netmask   => '255.255.255.0',
    }

# Add custom routes

file_line { 'route_line':
  line => "up route add -net 10.1.${number}.0 netmask 255.255.255.0 gw 10.2.${number}.2 dev eth1",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}

file_line { 'route_line2':
  line => "up route add -net 10.2.${number}.0 netmask 255.255.255.0 gw 10.2.${number}.2 dev eth1",
  path => '/etc/network/interfaces',
  require => File["/etc/network/interfaces"],
}