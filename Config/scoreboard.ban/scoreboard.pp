#Margus Ernits
#License MIT

        class apachesrv {
        class { 'apache':
                mpm_module => prefork,
        }

        class {'::apache::mod::php':
        }
        apache::vhost { 'scoreboard.ban':
                port    => '80',
                docroot => '/var/www/scoreboard.ban',
        } ->
        file { "/var/www/scoreboard.ban/index.php":
                ensure => present,
                content => "scoreboard.ban\n<br\n>Server tootab\n<?php phpinfo(); ?>",
        }
}

node 'scoreboard.ban' {
        include apachesrv
}
