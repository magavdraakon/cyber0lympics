<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
require_once DVWA_WEB_PAGE_TO_ROOT.'admin/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .='Weather forecast';
$page[ 'page_id' ] = 'xss_r';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'high':
	default:
		$vulnerabilityFile = 'high.php';
		break;
}

require_once DVWA_WEB_PAGE_TO_ROOT."subpage/weather/source/{$vulnerabilityFile}";


$page[ 'body' ] .= "
<div class=\"body_padded\">
	<h1>Weather Forecast</h1>

	<p>The Kingdom of Banania's Meterology Institute have developed one of the world's most accurate weather forecast.</p>
	<p>It can be tried out below</p>
	<p>Just enter the location in the following format - <strong>City,country</strong> in two digit format - e.g. <strong>London,uk</strong> or <strong>Tallinn,ee</strong> </p>

	<div class=\"boxform\">

		<form name=\"Weather\" action=\"#\" method=\"GET\">
			<p>Weather forecast </p>
			<input type=\"text\" name=\"name\">
			<input type=\"submit\" value=\"Submit\">
		</form>

		{$html}

	</div>

	<img src=\"".DVWA_WEB_PAGE_TO_ROOT."admin/images/Weather2.png\" alt=\"Long live the king!\" style=\"float:right;margin:0 350px 0px 0\" />


</div>
";

dvwaHtmlEcho( $page );

?>
