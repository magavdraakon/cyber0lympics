<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
require_once DVWA_WEB_PAGE_TO_ROOT.'admin/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= 'Banania visas';
$page[ 'page_id' ] = 'sqli';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'high':
	default:
		$vulnerabilityFile = 'high.php';
		break;
}

require_once DVWA_WEB_PAGE_TO_ROOT."subpage/visas/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'sqli';
$page[ 'source_button' ] = 'sqli';

$magicQuotesWarningHtml = '';

// Check if Magic Quotes are on or off
if( ini_get( 'magic_quotes_gpc' ) == true ) {
	$magicQuotesWarningHtml = "	<div class=\"warning\">Magic Quotes are on.</div>";
}

$page[ 'body' ] .= "
<div class=\"body_padded\">
	<h1>Banania Kingdom citizens </h1>
	Here You can have a look at some of the citizens that have been provided a visa by The Kingdom of Banania
	<br>
	<br>
	Just write the Visa ID into the box below
	<br>
	<br>
	{$magicQuotesWarningHtml}

	<div class=\"boxform\">

		<h3>Visa ID:</h3>

		<form action=\"#\" method=\"GET\">
			<input type=\"text\" name=\"id\">
			<input type=\"submit\" name=\"Submit\" value=\"Submit\">
		</form>

		{$html}

	</div>

	<h3>How to get Banania Kingdom visa</h3>

		<li>Donate atleast 3 tons of bananas to the kingdom</li>
		<li>Hail our king the Bananius XII Magnus</li>
		<li>Take good care of the wildlife and environment</li>
</div>

";

dvwaHtmlEcho( $page );

?>
