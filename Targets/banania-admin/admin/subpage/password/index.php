<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
require_once DVWA_WEB_PAGE_TO_ROOT.'admin/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= $page[ 'title_separator' ].'Vulnerability: Cross Site Request Forgery (CSRF)';
$page[ 'page_id' ] = 'csrf';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'high':
	default:
		$vulnerabilityFile = 'high.php';
		break;
}

require_once DVWA_WEB_PAGE_TO_ROOT."subpage/password/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'csrf';
$page[ 'source_button' ] = 'csrf';

$page[ 'body' ] .= "
<div class=\"body_padded\">
	<center><h1>Password change</h1></center>
	<p></p>
	<h2>Beware! </h2>
	<p>This is only for Administrators! <img src=\"".DVWA_WEB_PAGE_TO_ROOT."admin/images/Admin.png\" alt=\"Long live the king!\"  style=\"float:right;margin:0px 310px 0px 0\" /> </p>
	<p>Do not attempt to change password if You are not the main Administrator!</p>
	<p>Any wrong doing will be reported!<p>

	<div class=\"boxform\">
	
	<h3>Change password</h3>
    <form action=\"#\" method=\"GET\">";
	
	if (dvwaSecurityLevelGet() == 'low'){
		$page[ 'body' ] .= "Current password:<br>
		<input type=\"password\" AUTOCOMPLETE=\"off\" name=\"password_current\"><br>";
	}
    
$page[ 'body' ] .= "    New password:<br>
    <input type=\"password\" AUTOCOMPLETE=\"off\" name=\"password_new\"><br>
    Confirm new password: <br>
    <input type=\"password\" AUTOCOMPLETE=\"off\" name=\"password_conf\">
    <br>
    <input type=\"submit\" value=\"Change\" name=\"Change\">
    </form>
	
	{$html}

	</div>


</div>
";

dvwaHtmlEcho( $page );

?>