<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
require_once DVWA_WEB_PAGE_TO_ROOT.'admin/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= 'Admin Panel';
$page[ 'page_id' ] = 'brute';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'high':
	default:
		$vulnerabilityFile = 'high.php';
		break;
}

require_once DVWA_WEB_PAGE_TO_ROOT."subpage/adminpanel/source/{$vulnerabilityFile}";

$page[ 'body' ] .= "
<div class=\"body_padded\">
	<center><h1>Admin Panel login</h1></center>
	<p></p>
	<h2>Beware! </h2>
	<p>This is only for Administrators! <img src=\"".DVWA_WEB_PAGE_TO_ROOT."admin/images/Admin.png\" alt=\"Long live the king!\"  style=\"float:right;margin:0px 310px 0px 0\" /> </p>
	<p>Do not attempt to login unless You are one!</p>
	<p>Any wrong doing will be reported!<p>

	<div class=\"boxform\">

		<h2>Login</h2>

		<form action=\"#\" method=\"GET\">
			Username:<br><input type=\"text\" name=\"username\"><br> 
			Password:<br><input type=\"password\" AUTOCOMPLETE=\"off\" name=\"password\"><br>
			<input type=\"submit\" value=\"Login\" name=\"Login\">
		</form>

		{$html}

	</div>

</div>
";

dvwaHtmlEcho( $page );

?>