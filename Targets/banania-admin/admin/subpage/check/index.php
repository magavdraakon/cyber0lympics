<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
require_once DVWA_WEB_PAGE_TO_ROOT.'admin/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= 'Check services';
$page[ 'page_id' ] = 'exec';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'low.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'high':
	default:
		$vulnerabilityFile = 'high.php';
		break;
}

require_once DVWA_WEB_PAGE_TO_ROOT."subpage/check/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'exec';
$page[ 'source_button' ] = 'exec';

$page[ 'body' ] .= "
<div class=\"body_padded\">
	<h1>Check Banania Kingdom services</h1>
	<p>In here You can check different Banania Kingdom machines or services if they are up or down.</p>
	<p>Some of the services, machines are: <br></p>
	<li>Administrator workstation - kaliX.ban - 10.1.X.66</li> 
	<li>The Great Firewall of Banania - fwX.ban - 10.1.X.254</li> 
	<li>Internet ISP Router - routerX.ban - 10.80.65.X</li> <br>
	<li>Hosting machine - hostingX.ban - 10.1.X.200</li>
	<li>Banania Kingdom blog - blogX.ban - 10.1.X.201</li>
	<li>Banania Kingdom government website - govX.ban - 10.1.X.202 
	<li>Banania Kingdom online shop - shopX.ban - 10.1.X.203 </li><br>
	<li>Banania Kingdom e-news website - bandemiaX.ban - 10.1.X.212</li>
	<li>Banania Kingdom admin panel - banania-adminX.ban - 10.1.X.213</li>
	<br>
	<strong>NB! X corresponds to the Cyber Olympian number!</strong>
	<br>
	<br>

	<div class=\"boxform\">

		<h2>Check</h2>

		<p>Enter an Banania Kingdom IP address below:</p>
		<form name=\"ping\" action=\"#\" method=\"post\">
			<input type=\"text\" name=\"ip\" size=\"30\">
			<input type=\"submit\" value=\"submit\" name=\"submit\">
		</form>

		{$html}

";

dvwaHtmlEcho( $page );

?>