<?php

define( 'DVWA_WEB_PAGE_TO_ROOT', '../../' );
require_once DVWA_WEB_PAGE_TO_ROOT.'admin/includes/dvwaPage.inc.php';

dvwaPageStartup( array( 'phpids' ) );

$page = dvwaPageNewGrab();
$page[ 'title' ] .= 'Banania criminals';
$page[ 'page_id' ] = 'sqli_blind';

dvwaDatabaseConnect();

$vulnerabilityFile = '';
switch( $_COOKIE[ 'security' ] ) {
	case 'low':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'medium':
		$vulnerabilityFile = 'medium.php';
		break;

	case 'high':
	default:
		$vulnerabilityFile = 'high.php';
		break;
}

require_once DVWA_WEB_PAGE_TO_ROOT."subpage/criminals/source/{$vulnerabilityFile}";

$page[ 'help_button' ] = 'sqli_blind';
$page[ 'source_button' ] = 'sqli_blind';

$magicQuotesWarningHtml = '';

// Check if Magic Quotes are on or off
if( ini_get( 'magic_quotes_gpc' ) == true ) {
	$magicQuotesWarningHtml = "	<div class=\"warning\">Magic Quotes are on.</div>";
}

$page[ 'body' ] .= "
<div class=\"body_padded\">
	<h1>Banania Kingdom criminals</h1>
	Here You can have a look at some of the citizens that have been apprehended by The Kingdom of Banania
	<br>
	<br>
	This is high security area!
	<br>
	<br>
	Just write the criminal's ID into the box below
	<br>
	<br>
	{$magicQuotesWarningHtml}

	<div class=\"boxform\">

		<h3>User ID:</h3>

		<form action=\"#\" method=\"GET\">
			<input type=\"text\" name=\"id\">
			<input type=\"submit\" name=\"Submit\" value=\"Submit\">
		</form>

		{$html}

	</div>
	<h3>The 3 Amendments of the Kingdom of Banania</h3>

<p>Disobeying one of these amendments will get You into trouble! <img src=\"".DVWA_WEB_PAGE_TO_ROOT."admin/images/Jail2.png\" alt=\"Long live the king!\"  style=\"float:right;margin:0 100px 0px 0\" /></p>


		<li>No right to Steal bananas from the Kingdom's banana reserve</li>
		<li>No right to mock our king the Bananius XII Magnus </li>
		<li>Eating all the bananas and leaving nothing for the poor</li>



</div>
";

dvwaHtmlEcho( $page );

?>
