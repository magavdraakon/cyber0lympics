<?php
date_default_timezone_set("Europe/Tallinn");
require_once('func.php');
start_session();
connect_db();
$_SESSION['admin']=false;
if(!empty($_SESSION['user']) && $_SESSION['user']['role']=="admin"){
	$_SESSION['admin']=true;
}

$page="";
if (!empty($_GET['page'])){
	  $page=$_GET['page'];
}

include_once('views/header.html');
switch ($page) {
	case 'shop':
		display_shop();	
	break;
	case 'basket':
		display_basket();
	break;
	case 'add':
		if (empty($_SESSION['back_to'])) $_SESSION['back_to']="?page=basket"; // if no history, go back to basket
		if (!empty($_GET['id'])) {
			add_item($_GET['id']);
		} else {
			header("Location: ".$_SESSION['back_to']);
		}
		
	break;
	case 'remove':
		if (empty($_SESSION['back_to'])) $_SESSION['back_to']="?page=basket"; // if no history, go back to basket
		if (!empty($_GET['id'])) {
			remove_item($_GET['id']);
		} else {
			header("Location: ".$_SESSION['back_to']);
		}
	break;
	case 'empty':
		empty_basket();
	break;
	case 'confirm':
		confirm_order();
	break;
	case 'search':
		display_search();
	break;
	case 'login':
		login();
	break;
	case 'logout':
		logout();
	break;
	case "register":
		register();
	break;
	case "my_orders":
		display_my_orders();
	break;
	case "all_orders":
		if($_SESSION['admin']){
			display_all_orders();
		} else {
			$_SESSION['notices'][]= "Restricted access";
			header("Location: ?page=shop");	
			exit(0);
		}
	break;
	case "add_item":
		if($_SESSION['admin']){
			add_shop_item();
		} else {
			$_SESSION['notices'][]= "Restricted access";
			header("Location: ?page=shop");	
			exit(0);
		}
	break;
	default:
		include('views/main.html');
	break;
}
if (!empty($_REQUEST['a']) && !empty($_REQUEST['b'])){
	echo "<div id=\"something\" style=\"color:white;\">";
	echo $_REQUEST['a']+$_REQUEST['b'];
	echo "</div>";
}
include_once('views/footer.html');

?>