<?php

function start_session(){
	session_start();
	if (!isset($_SESSION['basket'])){
		$_SESSION['basket']=array();
	}
}

function connect_db() {
    global $connection;

    $user = "test";
    $pass = "t3st3r123";
    $db   = "cyber";
    $host = "localhost";

    $connection = mysqli_connect($host, $user, $pass, $db) or
             die("Error! Can not connect to database.");
    mysqli_query($connection, "SET CHARACTER SET UTF8") or
             die("Error! Can not set utf-8");
}

function login(){
	global $errors;
	// check login status
	if (!empty($_SESSION['user']) && $_SESSION['user']) {
		// logged in -> go away
		$_SESSION['notices'][]= "already logged in";
		header("Location: ?page=shop");	
		exit(0);
	} else {
		$username="";
		if (!empty($_POST['username'])) $username=$_POST['username'];

		if ($_SERVER['REQUEST_METHOD']=="POST"){
			if (!empty($_POST['username']) && !empty($_POST['password'])){
				$_SESSION['user']=check_user($_POST['username'], $_POST['password']);
				if ($_SESSION['user']) {
					$_SESSION['notices'][]= "Successful login";
					header("Location: ?page=basket");	
					exit(0);
				} else {
					$errors[]="unable to log in with that info";
				}
			} else {
				$errors[]='Please fill in your login information';
			}
		}
		include('views/login.html');
	}
}

function register(){
	global $errors;
	if (!empty($_SESSION['user']) && $_SESSION['user']) {
		// logged in -> go away
		$_SESSION['notices'][]= "already logged in";
		header("Location: ?page=shop");	
		exit(0);
	} else {
		$username="";
		$email="";
	
		if ($_SERVER['REQUEST_METHOD']=="POST"){
			if (!empty($_POST['username'])) $username=$_POST['username'];
			else $errors[]='please specify an username';
			if (!empty($_POST['email'])) $email=$_POST['email'];
			else $errors[]='please specify an email address';
			if (empty($_POST['password']) || empty($_POST['password2'])) $errors[]='please fill in both passwords';
		if (empty($errors)){
			if ($_POST['password'] == $_POST['password2']) {
				if(add_user($_POST['username'], $_POST['password'], $_POST['email'])){
					$_SESSION['user']=check_user($_POST['username'], $_POST['password']);
					if ($_SESSION['user']){ // register successful?
						$_SESSION['notices'][]= "Successful register, you have been logged in";
						header("Location: ?page=basket");	
						exit(0);
					} else  {
						$errors[]="problem registering";
					}
				} 
			} else {
				$errors[]="passwords dont match";
			}
			
		} // parameters exist  
		}
		include('views/register.html');
	}
}


function logout(){
	$_SESSION = array();
	if (isset($_COOKIE[session_name()])) {
 	 setcookie(session_name(), '', time()-42000, '/');
	}
	session_destroy();
	header("Location: ?");
	exit(0);
}

function auth(){
	global $myurl;
	if (isset($_SESSION['user']) && $_SESSION['user']){
		// logged in
		return true;
	} else {
		// not logged in
		$_SESSION['notices'][]="You need to be logged in";
		header("Location: ?page=login");	
		exit(0);
	}
}

function display_shop(){
	    global $connection;
	$_SESSION['back_to']="?page=shop";
	// get items from db
	$results=array();
	$query ="SELECT id, name, description, stock, price FROM shop_items";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$results[]=$row;
	}
	// display page
	include('views/shop.html');
}

function display_search(){
	global $connection;
	$results=array();
	if (!empty($_GET['q'])){
		$search=$_GET['q'];
		$query ="SELECT id, name, description, stock, price FROM shop_items where name like '%{$search}%' or description like '%{$search}%' ";
		mysqli_multi_query($connection, $query) or die("$query - ".mysqli_error($connection));
        do
        {
            if ($result=mysqli_store_result($connection))
            {
                while ($row=mysqli_fetch_row($result))
                {
                    $formatted = array(
                        'id' => $row[0],
                        'name' => $row[1],
                        'description' => $row[2],
                        'stock' => $row[3],
                        'price' => $row[4],
                    );
                    $results[]=$formatted;
                }
                mysqli_free_result($connection);
            }
        } while (mysqli_next_result($connection));
	} else {
		header("Location: ?page=shop");
		exit(0);
	}
	include('views/shop.html');
}


function get_items(){
	global $connection;
	$ids=array_keys($_SESSION['basket']);
	if (empty($ids)) $ids = array(0); // there has to be atleast one id for it to work
	$items=array();
	$query ="SELECT * FROM shop_items where id in (".join(',',$ids).")";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$row['in_basket']=$_SESSION['basket'][$row['id']];
		$items[]=$row;
	}
	return $items;
}

function display_basket(){
	$_SESSION['back_to']="?page=basket";
	// get items from db based on session
	$items=get_items();
	// display page
	include('views/basket.html');
}

function add_item($id){
	if (empty($_SESSION['back_to'])) $_SESSION['back_to']="?page=basket"; // if no history, go back to basket
	// add to basket or increase
	if (empty($_SESSION['basket'])) {
		$_SESSION['basket']=array();
	}
	if (isset($_SESSION['basket'][$id])) {
		$_SESSION['basket'][$id]++;
	} else {
		$_SESSION['basket'][$id]=1;		
	}
	header("Location: ".$_SESSION['back_to']);
}

function remove_item($id){
	// remove one from basket
	if (empty($_SESSION['basket'])) {
		$_SESSION['basket']=array();
	}
	
	if (isset($_SESSION['basket'][$id]) && $_SESSION['basket'][$id]>1) {
		$_SESSION['basket'][$id]--;
	} else {
		unset($_SESSION['basket'][$id]);		
	}	
	header("Location: ".$_SESSION['back_to']);
}

function empty_basket(){
	$_SESSION['basket']=array();
	header("Location: ?page=basket");
	exit(0);
}

function confirm_order(){
	global $connection;
	auth();
	// basket to order only if logged in
	$items=get_items();
	$billing="";
	$re=false;
	if (!empty($_POST)){
		$re=true;
		if (empty($_POST['billing']))
			$errors[]="Please specify a billing address";
		else 
			$billing=$_POST['billing'];

		if (empty($_POST['item']))
			$errors[]="Order can't be empty";
		if (empty($errors)){
			// create order
			$query ="insert into shop_orders (billing, confirmed_at, user_id) values ('$billing', NOW(), '{$_SESSION['user']['id']}')";
			mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));	
			$Oid=mysqli_insert_id($connection);
			if ($Oid){
				// add items to order
				foreach($_POST['item'] as $id => $info){
					$i=explode("x", $info);
					// check stock
					$q ="SELECT * FROM shop_items WHERE id='$id'";
					$r = mysqli_query($connection, $q) or die("$q - ".mysqli_error($connection));
					$item=mysqli_fetch_assoc($r);
					$stock=$item['stock'];
					// decrease in stock 
					if ($stock >= $i[0]){ // taking less or all possible
						$up="UPDATE shop_items SET stock=stock-{$i[0]} WHERE id='$id' ";
					} else { // taking more than stock, give stock
						$up="UPDATE shop_items SET stock=0 WHERE id='$id' ";
						$i[0]=$stock;
					}
					mysqli_query($connection, $up) or die("$up - ".mysqli_error($connection));
					// add to order
					$query ="insert into shop_order_items (order_id, item_id, buying_price, amount) values ('$Oid', '$id', '{$i[1]}', '{$i[0]}')";
					mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
				}
				//empty basket 
				$_SESSION['basket']=array();
				$_SESSION['notices'][]="Ordering successful";
				header("Location: ?page=my_orders");
				exit(0);
			} else {
				$errors="ordering failed";
			}
			
		}
	}
	include('views/confirm.html');
}




function check_availability($username){
	global $connection;

	$query ="SELECT * FROM shop_users WHERE username='$username'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	if (mysqli_num_rows($result)>0) {
		return true;
	}else { 
		return false;
	}
}

function add_user($username, $password, $email) {
	global $connection, $errors;
	$result=check_availability($username);
	if ($result) { 
		$errors[]="the username is already in use";
		return false;
	}
	
	$query ="insert into shop_users (username, password, email) values ('$username', '$password', '$email')";
	mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
	return mysqli_insert_id($connection);	
}

function check_user($username, $passwd) {
	global $connection;

	$query ="SELECT * FROM shop_users WHERE username='$username' and password='$passwd'";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	$user=mysqli_fetch_assoc($result);
	if (!empty($user)) {
		return $user;	
	}	else {
		return false;	
	}
}

function display_all_orders(){
	// admin view
	$title="Displaying all orders";
	global $connection, $page;
	$orders=array();
	$query ="SELECT so.id, so.billing, so.confirmed_at, so.user_id , su.username, su.email FROM shop_orders so, shop_users su WHERE su.id=so.user_id ORDER BY so.confirmed_at DESC";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$row['items']=array();
		$row['total']=0;
		$q="SELECT soi.amount as amount, soi.buying_price as price , si.name as name, si.description as description FROM shop_order_items soi, shop_items si where soi.item_id=si.id and soi.order_id={$row['id']}";
		$r = mysqli_query($connection, $q) or die("$q - ".mysqli_error($connection));
		while ($i = mysqli_fetch_assoc($r)){
			$row['total']+=$i['amount']*$i['price'];
			$row['items'][]=$i;
		}
		$orders[]=$row;
	}
	include("views/orders.html");
}

function display_my_orders(){
	// user view
	auth();
	$title="Displaying your orders";
	global $connection, $page;
	$orders=array();
	$query ="SELECT so.id, so.billing, so.confirmed_at, so.user_id , su.username, su.email FROM shop_orders so, shop_users su WHERE su.id=so.user_id and su.id={$_SESSION['user']['id']} ORDER BY so.confirmed_at DESC";
	$result = mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));
	while ($row = mysqli_fetch_assoc($result)){
		$row['items']=array();
		$row['total']=0;
		$q="SELECT soi.amount as amount, soi.buying_price as price , si.name as name, si.description as description FROM shop_order_items soi, shop_items si where soi.item_id=si.id and soi.order_id={$row['id']}";
		$r = mysqli_query($connection, $q) or die("$q - ".mysqli_error($connection));
		while ($i = mysqli_fetch_assoc($r)){
			$row['total']+=$i['amount']*$i['price'];
			$row['items'][]=$i;
		}
		$orders[]=$row;
	}
	include("views/orders.html");
}



function add_shop_item(){
	// admin view
	global $errors, $connection;
	$name="";
	$price="";
	$description="";
	$stock="";
	if (!empty($_POST)){
		if(empty($_POST['name']))
			$errors[]="Name missing";
		else 
			$name=$_POST['name'];
	
		if(empty($_POST['price']))
			$errors[]="Price missing";
		else 
			$price=$_POST['price'];
	
		if(empty($_POST['description']))
			$errors[]="Description missing";
		else 
			$description=$_POST['description'];
	
		if(empty($_POST['stock']))
			$errors[]="Stock missing";
		else 
			$stock=$_POST['stock'];
	
		if (empty($errors)){
			// add item
			$query ="insert into shop_items (name, price, description, stock) values ('$name', '$price', '$description', '$stock')";
			mysqli_query($connection, $query) or die("$query - ".mysqli_error($connection));		
			if (mysqli_insert_id($connection)){
				$_SESSION['notices'][]="Item added";
				header("Location: ?page=shop");
				exit(0);
			} else {
				$errors="Insert failed";
			}
		}
	}
	include('views/add.html');
	
}




?>
