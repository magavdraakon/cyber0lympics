DROP DATABASE IF EXISTS cyber;
CREATE DATABASE IF NOT EXISTS cyber;
USE cyber;

GRANT USAGE ON *.* TO 'test'@'localhost';
DROP USER 'test'@'localhost';
GRANT USAGE ON *.* TO 'test'@'%';
DROP USER 'test'@'%';

CREATE USER 'test'@'localhost' IDENTIFIED BY 't3st3r123';
GRANT ALL PRIVILEGES ON *.* TO 'test'@'localhost' WITH GRANT OPTION;
CREATE USER 'test'@'%' IDENTIFIED BY 't3st3r123';
GRANT ALL PRIVILEGES ON *.* TO 'test'@'%' WITH GRANT OPTION;

CREATE TABLE shop_items (
id integer PRIMARY KEY auto_increment,
name varchar(255),
price float(10,3),
description text,
stock integer
);

INSERT INTO `shop_items` (`id`, `name`, `price`, `description`, `stock`) VALUES
(1, 'BFSMALL', 15.000, 'Bananian flag, small', 4539921),
(2, 'BFLARGE', 230.000, 'Bananian flag, large', 2499234),
(3, 'BPINB', 33.000, 'Banania pin, bronze', 5263293),
(4, 'BPINS', 210.000, 'Banania pin, silver', 2299900),
(5, 'BPING', 1240.000, 'Banania pin, gold', 1212408),
(6, 'BOOZE', 120.000, 'Banana liqueur, 1 litre (beware, it is strong!)', 5689342),
(7, 'BMPIC', 3650.000, 'Portrait of King Bananius XII Magnus (oil on canvas)', 7720988),
(8, 'BSHIRT', 85.000, 'Banania T-shirt ("Bent Yellows rule!")', 8223723),
(9, 'BCSTICKER', 10.000, 'Banania car sticker (BAN)', 1401458),
(10, 'BCROC', 66.000, 'Banania National Crocodile plush toy', 2338291),
(11, 'BANANAS', 12.000, '1 kg of fresh Southwestern Boozer Bananas', 3223245);

CREATE TABLE shop_users(
id integer PRIMARY KEY auto_increment, 
username varchar(255) unique,
password varchar(255) not null,
email varchar(255),
role enum('user', 'admin') DEFAULT 'user'
);

INSERT INTO `shop_users` (`id`, `username`, `password`, `email`, `role`) VALUES
(1, 'test', 'testPW', 'some@one.com', 'user'),
(2, 'admin', 'adminPW', 'admin@thisShop.net', 'admin'),
(3, 'Unavailable', 'Banana123', 'none@banana.com', 'user'),
(4, 'Bananian1', 'somePassword', 'banana@national.com', 'user'),
(5, 'ThisIsNotAFruit', 'b2n2n2', 'some@other.com', 'user');

CREATE TABLE shop_orders (
id integer PRIMARY KEY auto_increment, 
billing text,
confirmed_at datetime,
user_id integer
);

INSERT INTO `shop_orders` (`id`, `billing`, `confirmed_at`, `user_id`) VALUES
(1, '123 Golden banana road\r\nBanania', '2015-01-12 16:56:11', 1),
(2, '98 Curved road\r\nBanania', '2015-01-28 15:17:07', 3),
(3, '98 Curved road\r\nBanania', '2015-02-01 19:19:44', 3),
(4, '1 Scale road\r\nBanania', '2015-02-05 06:48:40', 2),
(5, '44 Fruit road\r\nFruitlandia', '2015-02-06 15:27:07', 5);


CREATE TABLE shop_order_items (
id integer PRIMARY KEY auto_increment, 
order_id integer,
item_id integer,
buying_price float(10,3),
amount integer
);

INSERT INTO `shop_order_items` (`id`, `order_id`, `item_id`, `buying_price`, `amount`) VALUES
(1, 1, 1, 15.000, 3),
(2, 1, 2, 230.000, 4),
(3, 1, 3, 33.000, 1),
(4, 1, 4, 210.000, 1),
(5, 2, 2, 230.000, 7),
(6, 2, 3, 33.000, 8),
(7, 3, 1, 15.000, 21),
(8, 4, 4, 210.000, 1),
(9, 5, 1, 15.000, 1),
(10, 5, 3, 33.000, 1),
(11, 5, 4, 210.000, 1);
