# user interface for task progress

* NB! the file/folder placement is temporary

## json format

The expexted json format is explained here

In **script.js ** the variable **tasks_json** points to a json file in the following format
```json
{ 
	"taskID":{ 
		"title": "task title in the grid" , 
		"goal": "task goal in markdown", 
		"file": "json file with results for each team", 
		"points":  
		},  
	"2":{ 
		"title": "keep going" , 
		"goal": "reach 1000 points", 
		"file": "json/task2.json", 
		"points": 3 
	} 
}
```

In the tasks json file, the task **file** field points to a json file in the following format. The ClassName is one of pre-determined classes for different statuses (look below usage -> statuses)
```json
{
   "teamID": {
		"status": "ClassName",
		"up": 0, 
		"wounded": 0, 
		"down": 0
	},
    "2": {
    	"status":"done",
    	"up": 0, 
		"wounded": 2, 
		"down": 5
	}
}
```

In **script.js ** the variable **teams_json** points to a json file in the following format
```json
 {
 	"teamID": {
 		"name": "" , 
 		"pic": "picture location"
 		}, 
 	"2": {
 		"name":"Mai" , 
 		"pic": "pictures/images.jpg"
 		}
 }
```


## useage

Different sides to setting up the user interface are explained here.

#### statuses

Currently there are 4 statuses (more can be added straight to ** style.css**) that correspond directly to the status given in for each team in a json file

* done - green
* warning - orange
* danger - red
* none - grey

## TODO

* automatic updating on pageloead (button for now)
* extra status info in results
* bananabar application with uptime/downtime info ????
* different photo based on overall status?