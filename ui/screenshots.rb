#!/usr/bin/env ruby
require 'rubygems'
require "timeout"
require 'watir'
require "watir-webdriver"

goto= ARGV[0]
@time= ARGV[1].to_i

@browser1 = Watir::Browser.new :phantomjs
@browser1.goto(goto+"?11&1") #  first page
@browser2 = Watir::Browser.new :phantomjs
@browser2.goto(goto+"?11&2") #  second page

def gets_timeout( prompt, secs )
  puts
  print prompt + "[timeout=#{secs}secs]: "
  Timeout::timeout( secs ) { STDIN.gets.chomp }
rescue Timeout::Error
  puts "taking screenshot now"
  nil  # return nil if timeout
end

loop do 
  # some code here
	break if gets_timeout( "enter 'quit' to stop", @time)=="quit"
	# take screenshots 	
	@browser1.screenshot.save "screenshots/P1/"+Time.now.strftime("%Y-%m-%d-%H.%M.%S")+".png"
	@browser2.screenshot.save "screenshots/P2/"+Time.now.strftime("%Y-%m-%d-%H.%M.%S")+".png"

end

@browser1.close
@browser2.close
