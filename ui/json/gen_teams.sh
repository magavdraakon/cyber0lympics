#!/bin/bash
# Author Margus Ernits margus.ernits@gmail.com
#
# License The MIT License (MIT) (included in project tree)
#
# Skript generates content for teams.json file for 40 students
# Use ./gen_teams.sh > teams.json

echo '{'

for no in {1..40}
do

echo '  "'$no'": {'
echo '    "name":"CyberOlympian'$no'",'
echo '    "pic":"pictures/nicubunu-Hacker.png",'
echo '    "enabled":"true"'
echo '  },'

done
	

echo '}'