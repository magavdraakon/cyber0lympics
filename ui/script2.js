var teams_json= "json/teams.json";
var tasks_json= "json/tasks.json";

var tasks={}; // { task_nr:{ title: , goal: } }
var teams={}; // {team_nr: {name: , pic: , etc:}}
var results={}; // {team_nr: { task_nr : { status: , comment:  } } }
var prev_results={}; // to detect changes
// useful tool http://jsonlint.com/
var last_query="";
var per_page=6;
var tasks_parent="tasks";
var results_parent="results";
var build="build";
var tab_holder="tabs";

  var teamholder="";
  var taskHolder="";

var current_tab=1;//first tab by default
var teamWidth=Math.floor($(window).width()/per_page);

var timeout=10000; //milliseconds
var timeLoop="";

var max_points=0;
var tasks_for={};
/* 

2. get teams
1. get tasks
  draw elements
3. get statuses
  specify element class + extra content like score
  status changed: animate
4. re-check teams
  new team: add a team to the last page with all current tasks
  no new team: continue
  less teams: remove team, move teams to fill pages
    http://api.jquery.com/detach/
5. re-check tasks
  new tasks: add elemets to teams
  no new tasks: continue
  less tasks: remove elements from teams

6. back to step 3

*/

$( document ).ready( function(){
  if(window.location.search!=""){
    info=window.location.search.split("?")[1];
   
    if (parseFloat(info.split('&')[0])>0){
       console.log(info.split('&')[0], 'per page');
      per_page=parseFloat(info.split('&')[0]);
      teamWidth=Math.floor($(window).width()/per_page);
    }
    if (parseFloat(info.split('&')[1])>=0){
      console.log(info.split('&')[1], 'page');
      current_tab=parseFloat(info.split('&')[1]);
    }
  }
  start();
/*	

  $('#test').transition({rotateY: '90deg'}, 
      function (){
        $(this).removeClass('done').addClass('warning');
        $(this).transition({rotateY: '0deg'}); 
    });*/
});

$( window ).resize(function() {
  teamWidth=Math.floor($(window).width()/per_page);
  $('.team').css('width', teamWidth+"px");
});

function start(){
  load_teams(); // do it once and then every time interval
  timeLoop = window.setInterval(load_teams, timeout);
  console.log("started listening");
}
function stop(){
  window.clearInterval(timeLoop);
  console.log("stopped listening");
}
function toggle_reload(el){
  if (el.innerHTML=="Start"){
    timeLoop = window.setInterval(load_teams, timeout);
    el.innerHTML="Stop";
  } else {
    el.innerHTML="Start";
    window.clearInterval(timeLoop);
  }

}

function load_teams(){
  $.ajax({
      url: teams_json,
      cache: false,
      type: "GET",
      dataType: "json",
      ifModified:true,
  }).done(function(data) {
      if (typeof(data)!="undefined"){ // teamid muutusid
        teams=data;
        make_teams();
      } 
      load_tasks();
  });
}

function make_teams(){
  /* always remake the pages into build_results, but if possible, 
    move existing objects from results and reset their content  
  */
  tasks_for=[]; // assume that no new teams are made
  con=document.getElementById(build);
  con.innerHTML=""; //clear content
  counter=0;
  pnum=1;

  $.each(teams, function (teamID, info){
    if (typeof(info['enabled'])!="undefined" && info['enabled']) { // if team is enabled
      if (counter==per_page) counter=0;
      counter++;
      if (counter==1){
        var page=document.createElement('tbody');
        page.setAttribute('class', 'page page'+pnum);
        page.setAttribute('id', 'page'+pnum);

        teamholder=document.createElement('tr');
        teamholder.setAttribute('class', 'teamRow');
        page.appendChild(teamholder);

        taskHolder=document.createElement('tr');
        taskHolder.setAttribute('class', 'taskRow');
        page.appendChild(taskHolder);

        con.appendChild(page);
        pnum++;
      }

      // check if team already existed
      if ($('#team'+teamID).length) {
        // existed, move
        $('#team'+teamID+" .pic").attr('src', info['pic']);
        $('#team'+teamID+" .name").html(info['name']);
        // detatch
        var team=$('#team'+teamID).detach();
        var task=$('#tasksFor'+teamID).detach();
        // attach to new page
        team.appendTo("#"+build+" .page"+(pnum-1)+" .teamRow");
        task.appendTo("#"+build+" .page"+(pnum-1)+" .taskRow")
      } else {
        // nope, make new
        tasks_for.push(teamID);
        team=document.createElement('td');
        team.setAttribute('id', "team"+teamID);
        team.setAttribute('class', "team");
        team.data=teamID;
      
        img=document.createElement('img');
        img.setAttribute('class', 'pic');
        img.setAttribute('src', info['pic']);
        team.appendChild(img);

        nam=document.createElement('p');
        nam.setAttribute('class', 'name');
        nam.innerHTML=info['name'];
        team.appendChild(nam);

        num=document.createElement('span');
        num.setAttribute('class', 'number');
        num.innerHTML=teamID;
        team.appendChild(num);

        prog=document.createElement('div');
        prog.setAttribute('class', 'bar');
        prog.setAttribute('id', 'bar'+teamID);

        per=document.createElement('div');
        per.setAttribute('class', 'percent');

        has=document.createElement('div');
        has.setAttribute('class', 'has');
        has.innerHTML="<span>0</span>%";

        prog.appendChild(has);


        prog.appendChild(per);
        team.appendChild(prog);

        /*var tasks=document.createElement('div');
        tasks.setAttribute('class', 'tasks');
        team.appendChild(tasks);*/

        teamholder.appendChild(team);
        var tasks=document.createElement('td');
        tasks.setAttribute('class', 'tasks');
        tasks.setAttribute('id', 'tasksFor'+teamID);
        taskHolder.appendChild(tasks);

      }
    }  
  });
  // move to right place
  $('#'+results_parent).html($('#'+build).html());
  $('#'+build).html('');
  // make tabs
  con=document.getElementById(tab_holder);
  con.innerHTML="";
  for(i=1;i<pnum;i++){
    tab=document.createElement('div');
    tab.setAttribute('class', 'tab '+ (i==current_tab ? 'active':'') );
    tab.setAttribute('id', "tab"+i);
    tab.innerHTML="P"+i;
    tab.data=i;
    tab.onclick=function(){
      current_tab=this.data;
      show_page(current_tab);
    }
    con.appendChild(tab);
  }
  // specify column width
  $('.team').css('width', teamWidth+"px");

  if (current_tab>pnum-1){ 
    current_tab=pnum-1;
    //console.log('max page', pnum-1);
  }

  if (current_tab>=0) show_page(current_tab);
  
}

function load_tasks(){
  $.ajax({
      url: tasks_json,
      cache: false,
      type: "GET",
      dataType: "json",
      ifModified:true,

  }).done(function(data) {
      if (typeof(data)!="undefined") { // taskid muutusid
        tasks=data;
        draw_objectives();
        make_tasks();
        move_after_divider();
      }
     if(tasks_for.length) make_tasks();
     get_status();
  });
}

function move_after_divider(){
  $('.task').css('margin-top', '0px');//reset
   $(".divider + .task").each(function(){
    // get previous elment's height and set it as this elements margin-top
    $(this).css('margin-top', parseFloat($(this).prev().css('height'))+2*parseFloat($(this).prev().css('padding-top')));
  });
}

function make_tasks(){
  //take all teams that need to be remade
  counter=0;
  pnum=1;
 $.each(teams, function (teamID, info){
    // add all tasks
    team=document.getElementById(build);
    team.innerHTML="";
    $.each(tasks, function (taskID, content){
      if (typeof(content['active'])!="undefined" && content['active']) { 
        // check if task already existed
        if ($('#t'+teamID+"-"+taskID).length) {
          //yep, move and reset data
          $("#t"+teamID+"-"+taskID+" .title").html(content['title']);
          // detatch
          task=$('#t'+teamID+"-"+taskID).detach();
          // attach to new page
          task.appendTo("#"+build);
        } else {
          //nope, create new
          if (typeof(content['divider'])!='undefined' && content['divider']){
            // this is a divider
            task=document.createElement('div');
            task.setAttribute('id', "t"+teamID+"-"+taskID);
            task.setAttribute('class', 'divider');

            tex=document.createElement('div');
            tex.setAttribute('class', 'news');
            tex.innerHTML=content["title"];
            task.appendChild(tex);
          } else {
            task=document.createElement('div');
            task.setAttribute('id', "t"+teamID+"-"+taskID);
            if (typeof(results[teamID])!="undefined" && typeof(results[teamID][taskID])!="undefined"){
              task.setAttribute('class', results[teamID][taskID]["status"] +" task");
            } else {
              task.setAttribute('class', "task");
            }
            tex=document.createElement('div');
            tex.setAttribute('class', 'title');
            tex.innerHTML=content["title"];
            task.appendChild(tex);
                
            poi=document.createElement('div');
            poi.setAttribute('class', 'points_got');
            poi.setAttribute('id', "points"+teamID+"-"+taskID);
            poi.innerHTML="";
            if (typeof(content['status'])!="undefined"){
              if(content['status'].indexOf('u')>=0){
                poi.innerHTML+='<p class="up">Up: <span>0</span></p>'
              }
              if(content['status'].indexOf('w')>=0){
                poi.innerHTML+='<p class="wounded">Wounded: <span>0</span></p>'
              }
              if(content['status'].indexOf('d')>=0){
                poi.innerHTML+='<p class="down">Down: <span>0</span></p>'
              }
            }
            //poi.innerHTML='<p class="up">Up: <span>0</span></p><p class="wounded">Wounded: <span>0</span></p><p class="down">Down: <span>0</span></p>';
            task.appendChild(poi);
          }
          team.appendChild(task);
        }
      }
    });
    // move to the team
    $('#tasksFor'+teamID).html($('#'+build).html());
    $('#'+build).html('');
    
  });
tasks_for=[];
}

function tryParseJSON (jsonString){
    try {
        var o = JSON.parse(jsonString);
        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
        if (o && typeof o === "object" && o !== null) {
            return o;
        }
    }
    catch (e) { }

    return false;
};

function get_status(){
  //TODO! check for last modified
  size=Object.keys(tasks).length;
  counter=0;
  prev_results=results; // save backup
  results={}; // restart
  $.each(tasks, function (index, content){
    // only ask for files if a file is described and the task enabled
    if (typeof(content['file'])!="undefined" && typeof(content['active'])!="undefined" && content['active']){
     $.ajax({
       url: content['file'],
       cache: false,
        type: "GET",
        dataType: "text",
        statusCode: { // if file not found, continue on to the next one
          404: function() {
            counter++;
            if (counter==size)  draw_results();
          }
        }
      }).done(function(dataO) {
        data=tryParseJSON(dataO)
        //check for json format
        if (data) {// update/insert info 
                  $.each(data, function (team, info){
                    if (typeof(results[team])=="undefined") results[team]={};
                    results[team][index]= info;
                  });
        } else {
          console.log(content['file'], "is not valid JSON");
        }
        // display if last
        counter++;
        if (counter==size)  draw_results();
      });
    } else {
      // no file or disabled
      counter++;
      if (counter==size)  draw_results();
    }
    });
 
}

function draw_results(){
  // go over all teams and tasks, check if status has changed. fill task with extra info
  $.each(teams, function (teamID, info) {
    var points=0;
    if (typeof(info['enabled'])!='undefined' && info['enabled']){
     $.each(tasks, function (taskID, content) {
       if (typeof(content['divider'])=='undefined' || (typeof(content['divider'])!='undefined' && !content['divider'])){
        if (typeof(results[teamID])!="undefined" && typeof(results[teamID][taskID])!="undefined"){
          // set or transition class
          if (!$('#t'+teamID+"-"+taskID).hasClass(results[teamID][taskID]["status"])){
            // didnt have this class, animate
             $('#t'+teamID+"-"+taskID).transition({rotateY: '90deg'}, 
               function (){
                $(this).attr('class', results[teamID][taskID]["status"] +" task");
                $(this).transition({rotateY: '0deg'}); 
              });
          }
          var prev_up=0;
          var prev_down=0;
          var prev_wounded=0;
          // check if there are previous values
          if ($('#points'+teamID+"-"+taskID+' .up span').length){
            prev_up=parseFloat($('#points'+teamID+"-"+taskID+' .up span').html());// had up
          }
          if ($('#points'+teamID+"-"+taskID+' .down span').length){
            prev_down=parseFloat($('#points'+teamID+"-"+taskID+' .down span').html());// had down
          }
          if ($('#points'+teamID+"-"+taskID+' .wounded span').length){
            prev_wounded=parseFloat($('#points'+teamID+"-"+taskID+' .wounded span').html());// had wounded
          }
          //cleanup
          var point_holder=document.getElementById('points'+teamID+"-"+taskID);
          if (point_holder==null)console.log(point_holder, 'points'+teamID+"-"+taskID);
          point_holder.innerHTML="";
          // add up counter if exists
          var up=0;
          if (typeof(results[teamID][taskID]["up"])!="undefined"){
           point_holder.innerHTML+="<p class=\"up\">Up: <span>"+prev_up+"</span></p>";
           up=results[teamID][taskID]["up"];
           // if the points change animate numebr change with a little zoom
            if (prev_up!=up){
              //console.log("Up changed!", teamID, taskID, $('#points'+teamID+"-"+taskID+' .up span'));
              $('#points'+teamID+"-"+taskID+' .up span').transition({ scale: 2.2 },
                function(){
                 $('#points'+teamID+"-"+taskID+' .up span').html(up);
                  $('#points'+teamID+"-"+taskID+' .up span').transition({scale: 1});
                }
                );
            }
          }
          var wounded=0;
          if (typeof(results[teamID][taskID]["wounded"])!="undefined"){
            point_holder.innerHTML+="<p class=\"wounded\">Wounded: <span>"+prev_wounded+"</span></p>";
           wounded=results[teamID][taskID]["wounded"];
            // if the points change animate numebr change with a little zoom
            if (parseFloat($('#points'+teamID+"-"+taskID+' .wounded span').html())!=wounded){
              $('#points'+teamID+"-"+taskID+' .wounded span').transition({ scale: 2.2 },
                function(){
                  $('#points'+teamID+"-"+taskID+' .wounded span').html(wounded);
                  $('#points'+teamID+"-"+taskID+' .wounded span').transition({scale: 1});
                }
                );
            }
          }        
          var down=0;
          if (typeof(results[teamID][taskID]["down"])!="undefined"){
           point_holder.innerHTML+="<p class=\"down\">Down: <span>"+prev_down+"</span></p>";
           down=results[teamID][taskID]["down"];
           // if the points change animate numebr change with a little zoom
            if (parseFloat($('#points'+teamID+"-"+taskID+' .down span').html())!=down){
              $('#points'+teamID+"-"+taskID+' .down span').transition({ scale: 2.2 },
                function(){
                 $('#points'+teamID+"-"+taskID+' .down span').html(down);
                  $('#points'+teamID+"-"+taskID+' .down span').transition({scale: 1});
                }
                );
            }
          }
          
          //$('#points'+teamID+"-"+taskID+' .x').html(p);
          // TODO: change other info according to the new status        
        } else {
          // either divider or no result
            $('#t'+teamID+"-"+taskID).attr('class', "task");
        }
      } // eof is not divider
    }); // eof each task
  }// eof if enabled
     // set progress bar
   /*  var per=((points/max_points)*100).toFixed(2);
      $('#bar'+teamID+" .percent").css({width: now+"%"});
     */
  });// eof each team
 
}



function show_page(p){
  if (p==0){
    show_objectives();
    return;
  } else {
    $('.tab').removeClass('active');
    $("#tab"+p).addClass('active');
    $("#"+tasks_parent).hide();
    $("#"+results_parent).show();
    $('.page').hide();
    $('#page'+p).show();
    move_after_divider();
  }
}
function show_objectives(){
  current_tab=0;
  $('.tab').removeClass('active');
  $("#objTab").addClass('active');
  $("#"+tasks_parent).show();
  $("#"+results_parent).hide();
}

function draw_objectives(){
  con=document.getElementById(tasks_parent);
  con.innerHTML="";
   $.each(tasks, function (taskID, content){
     if (typeof(content['active'])!="undefined" && content['active']) { 
      var div=document.createElement('div');
      if (typeof(content['divider'])!='undefined' && content['divider']){
        div.setAttribute('class', 'news');
      } else {
        div.setAttribute('class', 'objective');
      }
      var h=document.createElement('h2');
      h.innerHTML=content['title'];
      div.appendChild(h);
      var ins=document.createElement('div');
      ins.setAttribute('class', 'marked');
      ins.innerHTML=content['goal'];
      div.appendChild(ins);
      con.appendChild(div);
    }
   });
  $(".marked").each(function(){
    $(this).html(marked($(this).text()));
  });
}


